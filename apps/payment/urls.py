from django.conf.urls import url

from .views import (WireCreateView, SystempayCreateView, PledgeProcessView,
                   PledgeCheckView, PledgeReturnView, RecuringProcessView,
                   RecuringCheckView, RecuringUpdateView, PledgeDeleteView)

urlpatterns = [
    url(
        r'wire/create/$',
        WireCreateView.as_view(),
        name='wire-create'
    ),
    url(
        r'pledge/create/$',
        SystempayCreateView.as_view(),
        name='pledge-create'
    ),
    url(
        r'pledge/(?P<pk>[0-9]+)/delete/$',
        PledgeDeleteView.as_view(),
        name='pledge-delete'
    ),
    url(
        r'pledge/(?P<pk>[0-9]+)/process/$',
        PledgeProcessView.as_view(),
        name='pledge-process',
    ),
    url(
        r'pledge/check/$',
        PledgeCheckView.as_view(),
        name='pledge-check',
    ),
    url(
        r"recuring/(?P<pk>[0-9]+)/process/$",
        RecuringProcessView.as_view(),
        name='recuring-process',
    ),
    url(
        r'recuring/check/$',
        RecuringCheckView.as_view(),
        name='recuring-check'
    ),
    url(
        r'recuring/(?P<pk>[0-9]+)/update/$',
        RecuringUpdateView.as_view(),
        name='recuring-update',
    ),
    url(
        r'pledge/(?P<pk>[0-9]+)/return/$',
        PledgeReturnView.as_view(),
        name='pledge-return'
    ),
]
