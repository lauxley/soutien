import pytz
from datetime import date

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.shortcuts import get_current_site
from django.db.utils import IntegrityError
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, CreateView, DetailView
from django.views.generic.edit import ProcessFormView

from user.models import Pledge, Donor

from .models import WireTransferProcessor, SystempayProcessor
from .forms import (WireTransferForm,
                    WireTransferFormHelper,
                    SystempayProcessForm,
                    SystempayValidateForm,
                    RecuringProcessForm,
                    RecuringValidateForm,
                    PledgeCreateForm,
                    PledgeDeleteForm)


class WireCreateView(PermissionRequiredMixin,
                 TemplateView, ProcessFormView):
    """
    This view is used to create WireTransfer pledges. It mainly display a form
    and validate it before creating pledges.
    """

    template_name = 'wire-create.html'
    permission_required = 'user.is_manager'
    form = WireTransferForm

    def get_context_data(self, **kwargs):
        context = super(WireCreateView, self).get_context_data(**kwargs)
        context['form'] = self.form()
        context['formhelper'] = WireTransferFormHelper()
        return context

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)

        if form.is_valid():
            data = form.cleaned_data
            # @FIXME: Try to work the SingleTon model on Proxy class maybe.
            # We should have only one WireTransferProcessor
            processor = WireTransferProcessor.objects.all()[0]
            pledge = Pledge(value=data['amount'],
                            pseudo=data['pseudo'],
                            donor=data.get('donor', None)
                            )
            try:
                pledge.process(processor)
                pledge.verify(**data)
            except:
                pledge.error()
            finally:
                pledge.save()
        return redirect('wire-create', permanent=False)


class SystempayCreateView(CreateView):
    model = Pledge
    form_class = PledgeCreateForm

    def get_success_url(self):
        pledge = self.object
        if pledge.recurring > 0:
            # We're recuring
            return reverse(r'recuring-process', kwargs={'pk': pledge.id})
        return reverse(r'pledge-process', kwargs={'pk': pledge.id})

    def get_initial(self):
        initial = super(SystempayCreateView, self).get_initial()
        if self.request.user.is_authenticated:
            # We have a user connected, let's use it to fill in the blank
            donor = Donor.objects.get(user=self.request.user)
            initial['email'] = donor.user.email
            initial['pseudo'] = donor.user.username
        processor = SystempayProcessor.objects.all()[0]
        initial['content_type'] = ContentType.objects.get_for_model(
            SystempayProcessor, for_concrete_model=False)
        initial['object_id'] = processor.id
        return initial

    def get_form_kwargs(self):
        kwargs = super(SystempayCreateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        pledge = form.instance
        if self.request.user.is_authenticated:
            pledge.donor = Donor.objects.get(user=self.request.user)
        else:
            # no user present, let's try to find out if we already have one
            # for our email address
            user = None
            try:
                user = User.objects.create_user(username=pledge.pseudo,
                                                password=form.cleaned_data['password'],
                                                email=form.cleaned_data['email'])
            except IntegrityError:
                user = User.objects.get(username=pledge.pseudo)
            pledge.donor, created = Donor.objects.get_or_create(user=user)
        return super(SystempayCreateView, self).form_valid(form)


class PledgeProcessView(DetailView, ProcessFormView):
    model = Pledge
    form = SystempayProcessForm

    def get_context_data(self, **kwargs):
        context = super(PledgeProcessView, self).get_context_data(**kwargs)
        form = self.form(initial=self.get_initial())
        pledge = self.get_object()
        config = pledge.processor.config
        form.compute_signature(config['certificate'])
        form.helper = FormHelper()
        form.helper.form_action = config['url']
        form.helper.form_method = 'POST'

        form.helper.add_input(Submit('submit',
                                     'Valider ce don',
                                     css_class='medium-blue-bg'
                                     ))
        context['form'] = form
        context['config'] = config
        try:
            pledge.process(pledge.processor)
        except Exception as e:
            pledge.error()
        finally:
            pledge.save(update_fields=['state', 'value', ])
        return context

    def get_initial(self):
        initial = {}
        pledge = self.get_object()
        config = pledge.processor.config
        site = get_current_site(self.request)
        initial['vads_site_id'] = config['shop_id']
        initial['vads_ctx_mode'] = config['mode']
        initial['vads_trans_id'] = '{:0>6}'.format(pledge.pk)

        trans_date = timezone.now().astimezone(pytz.timezone('UTC'))
        initial['vads_trans_date'] = trans_date.strftime('%Y%m%d%H%M%S')
        initial['vads_amount'] = int(pledge.value * 100)
        initial['vads_page_action'] = 'PAYMENT'
        initial['vads_payment_config'] = 'SINGLE'
        initial['vads_url_return'] = 'https://' + site.domain + reverse(
            'pledge-return',
            kwargs={'pk': pledge.pk})
        initial['vads_shop_name'] = site.name
        initial['vads_shop_url'] = 'https://' + site.domain
        initial['vads_url_check'] = 'https://' + site.domain + reverse(
            'pledge-check')
        return initial


class RecuringProcessView(DetailView, ProcessFormView):
    model = Pledge
    form = RecuringProcessForm

    def get_context_data(self, **kwargs):
        context = super(RecuringProcessView, self).get_context_data(**kwargs)
        form = self.form(initial=self.get_initial())
        pledge = self.get_object()
        config = pledge.processor.config
        form.compute_signature(config['certificate'])
        form.helper = FormHelper()
        form.helper.form_action = config['url']
        form.helper.form_method = 'POST'
        form.helper.add_input(Submit('submit', 'Valider ce don'))
        context['form'] = form
        context['config'] = config
        try:
            pledge.process(pledge.processor)
        except Exception as e:
            pledge.error()
        finally:
            pledge.save(update_fields=['state', 'recurring'])
        return context

    def get_initial(self):
        initial = {}
        pledge = self.get_object()
        config = pledge.processor.config
        site = get_current_site(self.request)
        initial['vads_site_id'] = config['shop_id']
        initial['vads_ctx_mode'] = config['mode']
        initial['vads_trans_id'] = '{:0>6}'.format(pledge.pk)

        trans_date = timezone.now().astimezone(pytz.timezone('UTC'))
        initial['vads_trans_date'] = trans_date.strftime('%Y%m%d%H%M%S')
        initial['vads_sub_amount'] = int(pledge.recurring) * 100
        initial['vads_page_action'] = 'REGISTER_SUBSCRIBE'
        initial['vads_identifier'] = '_'.join([trans_date.strftime('%Y%m%d%H%M%S'),
                pledge.donor.user.username])
        initial['vads_cust_email'] = pledge.donor.user.email
        initial['vads_sub_effect_date'] = trans_date.strftime('%Y%m%d')
        initial['vads_url_return'] = 'https://' + site.domain + reverse(
            'pledge-return',
            kwargs={'pk': pledge.pk})
        initial['vads_shop_name'] = site.name
        initial['vads_shop_url'] = 'https://' + site.domain
        initial['vads_url_check'] = 'https://' + site.domain + reverse(
            'recuring-check')
        return initial


class PledgeReturnView(DetailView, ProcessFormView):
    model = Pledge
    template_name = 'systempay_check.html'
    form_class = PledgeCreateForm
    success_url = r'/perso/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

    def get_context_data(self, *args, **kwargs):
        context = super(PledgeReturnView, self).get_context_data(*args,
                                                                 **kwargs)
        auth_form = AuthenticationForm()
        auth_form.helper = FormHelper()
        auth_form.helper.form_action = reverse('login')
        auth_form.helper.add_input(Submit('submit', 'Submit'))
        context["account_login"] = auth_form
        return context


class PledgeDeleteView(ProcessFormView):
    form_class = PledgeDeleteForm
    success_url = r'/perso/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        form.instance = Pledge.objects.get(pk=kwargs['pk'])
        if form.is_valid():
            form.instance.delete()
        return redirect('perso-home')


@method_decorator(csrf_exempt, name='dispatch')
class RecuringCheckView(ProcessFormView):
    form_class = RecuringValidateForm
    success_url = r'/payment/{pk}/process/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        try:
            pledge = Pledge.objects.get(transaction=request.POST['vads_trans_id'])
        except Pledge.DoesNotExist:
            # We need to be a bit more funkier, see issue #127
            identifier = request.POST['vads_identifier']
            date_creation = date(int(identifier[0:4]),
                                 int(identifier[4:6]),
                                 int(identifier[6:8]))
            username = identifier.split('_', 1)[1]
            amount = int(request.POST['vads_sub_amount'])/100
            try:
                pledge = Pledge.objects.get(date_creation=date_creation,
                                           donor__user__username=username,
                                           recurring=amount)
            except Pledge.MultipleObjectsReturned:
                pledge = Pledge.objects.filter(date_creation=date_creation,
                                               donor__user__username=username,
                                               recurring__gt=amount)[0]
            pledge.transaction = identifier
            pledge.save()
        config = pledge.processor.config

        if form.is_valid():
            signature = form.compute_signature(config['certificate'])
            provided_signature = form.cleaned_data['signature']
            try:
                assert signature == provided_signature
            except:
                pledge.error()
                pledge.save(update_fields=['state', ])
                return redirect('pledge-return', pk=pledge.pk)
            try:
                pledge.verify(**form.cleaned_data)
            except Exception as e:
                pledge.error()
            finally:
                pledge.save(update_fields=['state', 'recurring'])
        else:
            pledge.error()
            pledge.save(update_fields=['state', ])
        return redirect('pledge-return', pk=pledge.pk)


@method_decorator(csrf_exempt, name='dispatch')
class PledgeCheckView(ProcessFormView):
    form_class = SystempayValidateForm
    succes_url = r'/payment/{pk}/process/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        pledge = Pledge.objects.get(pk=request.POST['vads_trans_id'])
        config = pledge.processor.config

        if form.is_valid():
            signature = form.compute_signature(config['certificate'])
            provided_signature = form.cleaned_data['signature']
            try:
                assert signature == provided_signature
            except:
                pledge.error()
                pledge.save(update_fields=['state', ])
                return redirect('pledge-return', pk=pledge.pk)

            try:
                pledge.verify(**form.cleaned_data)
            except Exception as e:
                pledge.error()
            finally:
                pledge.save(update_fields=['state', 'value', ])
        else:
            if pledge:
                pledge.error()
                pledge.save(update_fields=['state', ])

        return redirect('pledge-return', pk=pledge.pk)


@method_decorator(csrf_exempt, name='dispatch')
class RecuringUpdateView(ProcessFormView, DetailView):
    pass
