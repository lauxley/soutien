# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (PaymentProcessor,
        WireTransferProcessor, SystempayProcessor)


@admin.register(PaymentProcessor)
class PaymentProcessorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'configs')
    search_fields = ('name',)


@admin.register(SystempayProcessor)
class SystempayProcessorProcessorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'configs')
    search_fields = ('name',)


@admin.register(WireTransferProcessor)
class WireTransferProcessorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'configs')
    search_fields = ('name',)
