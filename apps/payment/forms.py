from hashlib import sha1

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (Layout, HTML, Submit, Div, Field)
from crispy_forms.bootstrap import InlineRadios
from django.urls import reverse
from django import forms
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from user.models import Donor, Pledge


class WireTransferForm(forms.Form):

    #: This is the amount of the wiretransfer. Will be mapped to Pledge.value
    amount = forms.FloatField(required=True)

    #: This is the pseudo/email/whatever associated to the wire transfert.
    pseudo = forms.CharField(required=False)

    #: This is the donor associated to the pledge, can be None
    donor = forms.ModelChoiceField(Donor.objects.all(), required=False)

    #: The non mandatory transaction_id is used to match pledge and bank
    transaction_id = forms.CharField(required=False)


class WireTransferFormHelper(FormHelper):
    """
    Formset used to implement a form to create a WireTransfer pledge.
    """
    def __init__(self, *args, **kwargs):
        super(WireTransferFormHelper, self).__init__(*args, **kwargs)
        self.form_action = 'wire-pledge'
        self.layout = Layout(
            HTML("Create a wire transfered pledge"),
            'amount',
            'pseudo',
            'donor',
            'transaction_id',
            Submit('submit', 'Create'),
        )


class SystempayFormMixin(forms.Form):
    def compute_signature(self, certificate):
        """
        We're computing the signature of teh form. It is the concatenation of
        all form fields starting by vads_ and alphabetically sorted with a +
        sign and the certificate added at the end.

        We  store the value in the signature field
        """
        to_hash = '+'.join([str(self[f].value()) for f in sorted(self.fields) if
                            f.startswith('vads_')])
        to_hash += '+{}'.format(certificate)
        signature = sha1()
        signature.update(to_hash.encode())
        self.fields['signature'].initial = signature.hexdigest()
        return signature.hexdigest()


class SystempayProcessForm(SystempayFormMixin):
    """
    This form is to send notification to the Systempay system. There's a lot of
    parameters, so we provide sane defaults.
    """
    vads_action_mode = forms.CharField(max_length=20,
                                       widget=forms.HiddenInput,
                                       initial='INTERACTIVE')
    vads_amount = forms.IntegerField(widget=forms.HiddenInput)
    vads_capture_delay = forms.IntegerField(initial=0,
                                            widget=forms.HiddenInput)
    vads_ctx_mode = forms.CharField(max_length=10,
                                    widget=forms.HiddenInput)
    vads_currency = forms.IntegerField(widget=forms.HiddenInput,
                                       initial=978)
    vads_language = forms.CharField(max_length=2,
                                    initial='fr',
                                    widget=forms.HiddenInput)
    vads_page_action = forms.CharField(max_length=20,
                                       widget=forms.HiddenInput)
    vads_payment_config = forms.CharField(max_length=100,
                                          widget=forms.HiddenInput)
    vads_redirect_error_timeout = forms.IntegerField(initial=5,
                                                     widget=forms.HiddenInput)
    vads_redirect_success_timeout = forms.IntegerField(initial=5,
                                                       widget=forms.HiddenInput)
    vads_return_mode = forms.CharField(max_length=5,
                                       initial='GET',
                                       widget=forms.HiddenInput)
    vads_shop_name = forms.CharField(max_length=100,
                                     widget=forms.HiddenInput)
    vads_shop_url = forms.URLField(widget=forms.HiddenInput)
    vads_site_id = forms.CharField(max_length=20,
                                   widget=forms.HiddenInput)
    vads_trans_date = forms.CharField(max_length=16,
                                      widget=forms.HiddenInput)
    vads_trans_id = forms.CharField(max_length=20,
                                    widget=forms.HiddenInput)
    vads_url_check = forms.URLField(widget=forms.HiddenInput)
    vads_url_return = forms.URLField(widget=forms.HiddenInput)
    vads_validation_mode = forms.IntegerField(initial=0,
                                              widget=forms.HiddenInput)
    vads_version = forms.CharField(max_length=2,
                                   initial='V2',
                                   widget=forms.HiddenInput)

    # Signature
    signature = forms.CharField(max_length=32, widget=forms.HiddenInput, initial='')


class SystempayValidateForm(SystempayFormMixin):
    """
    This form is received when a payment form systempay needs validation.
    The QueryDict received is as follow:
        'vads_action_mode': ['INTERACTIVE'],
        'vads_amount': ['500'],
        'vads_auth_mode': ['FULL'],
        'vads_auth_number': ['3fec2d'],
        'vads_auth_result': ['00'],
        'vads_bank_code': ['17807'],
        'vads_bank_product': ['F'],
        'vads_capture_delay': ['0'],
        'vads_card_brand': ['CB'],
        'vads_card_country': ['FR'],
        'vads_card_number': ['497010XXXXXX0014'],
        'vads_contract_used': ['5201306'],
        'vads_ctx_mode': ['TEST'],
        'vads_currency': ['978'],
        'vads_effective_amount': ['500'],
        'vads_effective_creation_date': ['20170919122202'],
        'vads_effective_currency': ['978'],
        'vads_expiry_month': ['6'],
        'vads_expiry_year': ['2018'],
        'vads_extra_result': [''],
        'vads_hash': ['74026bd3287e4241f30b1eac74ae6951a525989b57c320750dedbbbbafac4582']}
        'vads_language': ['fr'],
        'vads_operation_type': ['DEBIT'],
        'vads_page_action': ['PAYMENT'],
        'vads_payment_certificate': ['3a179852ac1bb4b7c64722d1b20c4052e20cc596'],
        'vads_payment_config': ['SINGLE'],
        'vads_payment_src': ['EC'],
        'vads_pays_ip': ['FR'],
        'vads_presentation_date': ['20170919122202'],
        'vads_result': ['00'],
        'vads_sequence_number': ['1'],
        'vads_shop_name': ['Support La Quadrature du Net'],
        'vads_shop_url': ['http://soutien.dev.laquadrature.net'],
        'vads_site_id': ['56499289'],
        'vads_threeds_cavv': ['Q2F2dkNhdnZDYXZ2Q2F2dkNhdnY='],
        'vads_threeds_cavvAlgorithm': ['2'],
        'vads_threeds_eci': ['05'],
        'vads_threeds_enrolled': ['Y'],
        'vads_threeds_error_code': [''],
        'vads_threeds_exit_status': ['10'],
        'vads_threeds_sign_valid': ['1'],
        'vads_threeds_status': ['Y'],
        'vads_threeds_xid': ['VkZHZVViWXpscWRDQkRjdWNuakE='],
        'vads_trans_date': ['20170919122200'],
        'vads_trans_id': ['000024'],
        'vads_trans_status': ['AUTHORISED'],
        'vads_trans_uuid': ['6c673fec09d047bea48b8ac137c7d7e4'],
        'vads_url_check_src': ['PAY'],
        'vads_validation_mode': ['0'],
        'vads_version': ['V2'],
        'vads_warranty_result': ['YES'],
        'signature': ['0fe90da61ec88b0ad2aae8d3367b5650150950af'],
    """
    # We cast everything to a char.

    vads_action_mode = forms.CharField(max_length=50,
                                       empty_value='',
                                       required=False)
    vads_amount = forms.CharField(max_length=50)
    vads_auth_mode = forms.CharField(max_length=50)
    vads_auth_number = forms.CharField(max_length=50)
    vads_auth_result = forms.CharField(max_length=50)
    vads_bank_code = forms.CharField(max_length=50,
                                     empty_value='',
                                     required=False)
    vads_bank_product = forms.CharField(max_length=50)
    vads_capture_delay = forms.CharField(max_length=50)
    vads_card_brand = forms.CharField(max_length=50)
    vads_card_country = forms.CharField(max_length=50)
    vads_card_number = forms.CharField(max_length=50)
    vads_contract_used = forms.CharField(max_length=50)
    vads_ctx_mode = forms.CharField(max_length=50)
    vads_currency = forms.CharField(max_length=50)
    vads_effective_amount = forms.CharField(max_length=50)
    vads_effective_creation_date = forms.CharField(max_length=50)
    vads_effective_currency = forms.CharField(max_length=50)
    vads_expiry_month = forms.CharField(max_length=50)
    vads_expiry_year = forms.CharField(max_length=50)
    vads_extra_result = forms.CharField(max_length=50,
                                        empty_value='',
                                        required=False)
    vads_hash = forms.CharField(max_length=100)
    vads_language = forms.CharField(max_length=50)
    vads_operation_type = forms.CharField(max_length=50)
    vads_page_action = forms.CharField(max_length=50,
                                       empty_value='',
                                       required=False)
    vads_payment_certificate = forms.CharField(max_length=50)
    vads_payment_config = forms.CharField(max_length=50,
                                          required=False,
                                          empty_value='')
    vads_payment_src = forms.CharField(max_length=50)
    vads_pays_ip = forms.CharField(max_length=50)
    vads_presentation_date = forms.CharField(max_length=50)
    vads_result = forms.CharField(max_length=50)
    vads_sequence_number = forms.CharField(max_length=50)
    vads_shop_name = forms.CharField(max_length=250,
                                     required=False,
                                     empty_value='')
    vads_shop_url = forms.CharField(max_length=250,
                                    required=False,
                                    empty_value='')
    vads_site_id = forms.CharField(max_length=50)
    vads_threeds_cavv = forms.CharField(max_length=50)
    vads_threeds_cavvAlgorithm = forms.CharField(max_length=50)
    vads_threeds_eci = forms.CharField(max_length=50)
    vads_threeds_enrolled = forms.CharField(max_length=50)
    vads_threeds_error_code = forms.CharField(max_length=50,
                                              empty_value='',
                                              required=False)
    vads_threeds_exit_status = forms.CharField(max_length=50)
    vads_threeds_sign_valid = forms.CharField(max_length=50)
    vads_threeds_status = forms.CharField(max_length=50)
    vads_threeds_xid = forms.CharField(max_length=50)
    vads_trans_date = forms.CharField(max_length=50)
    vads_trans_id = forms.CharField(max_length=50)
    vads_trans_status = forms.CharField(max_length=50)
    vads_trans_uuid = forms.CharField(max_length=50)
    vads_url_check_src = forms.CharField(max_length=50)
    vads_validation_mode = forms.CharField(max_length=50)
    vads_version = forms.CharField(max_length=50)
    vads_warranty_result = forms.CharField(max_length=50)
    #: Signature
    signature = forms.CharField(max_length=40)


class RecuringProcessForm(SystempayFormMixin):
    vads_action_mode = forms.CharField(max_length=20,
                                       widget=forms.HiddenInput,
                                       initial='INTERACTIVE')
    vads_ctx_mode = forms.CharField(max_length=10, widget=forms.HiddenInput)
    vads_cust_email = forms.EmailField(widget=forms.HiddenInput)
    vads_identifier = forms.CharField(widget=forms.HiddenInput,
                                      max_length=50)
    vads_language = forms.CharField(max_length=2,
                                    initial='fr',
                                    widget=forms.HiddenInput)
    vads_page_action = forms.CharField(max_length=20,
                                       widget=forms.HiddenInput)
    vads_redirect_error_timeout = forms.IntegerField(initial=5,
                                                     widget=forms.HiddenInput)
    vads_redirect_success_timeout = forms.IntegerField(initial=5,
                                                       widget=forms.HiddenInput)
    vads_return_mode = forms.CharField(max_length=5,
                                       initial='GET',
                                       widget=forms.HiddenInput)
    vads_shop_name = forms.CharField(max_length=100,
                                     widget=forms.HiddenInput)
    vads_shop_url = forms.URLField(widget=forms.HiddenInput)
    vads_site_id = forms.CharField(max_length=20,
                                   widget=forms.HiddenInput)
    vads_sub_amount = forms.IntegerField(widget=forms.HiddenInput)
    vads_sub_currency = forms.IntegerField(widget=forms.HiddenInput,
                                           initial=978)
    vads_sub_desc = forms.CharField(widget=forms.HiddenInput,
                                    initial='RRULE:FREQ=MONTHLY;COUNT=120;BYMONTHDAY=8',
                                    max_length=50)
    vads_sub_effect_date = forms.CharField(widget=forms.HiddenInput,
                                           max_length=10)
    vads_trans_date = forms.CharField(max_length=16,
                                      widget=forms.HiddenInput)
    vads_trans_id = forms.CharField(max_length=20,
                                    widget=forms.HiddenInput)
    vads_url_check = forms.URLField(widget=forms.HiddenInput)
    vads_url_return = forms.URLField(widget=forms.HiddenInput)
    vads_validation_mode = forms.IntegerField(initial=0,
                                              widget=forms.HiddenInput)
    vads_version = forms.CharField(max_length=2,
                                   initial='V2',
                                   widget=forms.HiddenInput)

    # Signature
    signature = forms.CharField(max_length=64, widget=forms.HiddenInput)


class RecuringValidateForm(SystempayFormMixin):
    vads_action_mode = forms.CharField(max_length=80)
    vads_amount = forms.CharField(max_length=80, empty_value='',
                                  required=False)
    vads_auth_mode = forms.CharField(max_length=80)
    vads_auth_number = forms.CharField(max_length=80)
    vads_auth_result = forms.CharField(max_length=80)
    vads_bank_code = forms.CharField(max_length=80, empty_value='',
                                     required=False)
    vads_bank_product = forms.CharField(max_length=80)
    vads_capture_delay = forms.CharField(max_length=80)
    vads_card_brand = forms.CharField(max_length=80)
    vads_card_country = forms.CharField(max_length=80)
    vads_card_number = forms.CharField(max_length=80)
    vads_contract_used = forms.CharField(max_length=80)
    vads_ctx_mode = forms.CharField(max_length=80)
    vads_currency = forms.CharField(max_length=80)
    vads_cust_email = forms.CharField(max_length=80)
    vads_effective_amount = forms.CharField(max_length=80,
                                            required=False)
    vads_effective_currency = forms.CharField(max_length=80)
    vads_expiry_month = forms.CharField(max_length=80)
    vads_expiry_year = forms.CharField(max_length=80)
    vads_extra_result = forms.CharField(max_length=80,
                                        required=False)
    vads_hash = forms.CharField(max_length=80)
    vads_identifier = forms.CharField(max_length=80)
    vads_identifier_status = forms.CharField(max_length=80)
    vads_language = forms.CharField(max_length=80)
    vads_page_action = forms.CharField(max_length=80)
    vads_payment_certificate = forms.CharField(max_length=80,
                                               required=False)
    vads_payment_src = forms.CharField(max_length=80)
    vads_pays_ip = forms.CharField(max_length=80)
    vads_recurrence_status = forms.CharField(max_length=80)
    vads_result = forms.CharField(max_length=80)
    vads_risk_control = forms.CharField(max_length=80,
                                        required=False)
    vads_shop_name = forms.CharField(max_length=80)
    vads_shop_url = forms.CharField(max_length=80)
    vads_site_id = forms.CharField(max_length=80)
    vads_sub_amount = forms.CharField(max_length=80)
    vads_sub_currency = forms.CharField(max_length=80)
    vads_sub_desc = forms.CharField(max_length=80)
    vads_sub_effect_date = forms.CharField(max_length=80)
    vads_subscription = forms.CharField(max_length=80)
    vads_threeds_cavv = forms.CharField(max_length=80)
    vads_threeds_cavvAlgorithm = forms.CharField(max_length=80)
    vads_threeds_eci = forms.CharField(max_length=80)
    vads_threeds_enrolled = forms.CharField(max_length=80)
    vads_threeds_error_code = forms.CharField(max_length=80,
                                              required=False)
    vads_threeds_exit_status = forms.CharField(max_length=80)
    vads_threeds_sign_valid = forms.CharField(max_length=80)
    vads_threeds_status = forms.CharField(max_length=80)
    vads_threeds_xid = forms.CharField(max_length=80)
    vads_trans_date = forms.CharField(max_length=80)
    vads_trans_id = forms.CharField(max_length=80)
    vads_url_check_src = forms.CharField(max_length=80)
    vads_validation_mode = forms.CharField(max_length=80)
    vads_version = forms.CharField(max_length=80)
    vads_warranty_result = forms.CharField(max_length=80)

    # Signature field
    signature = forms.CharField(max_length=80)


class PledgeCreateFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(PledgeCreateFormHelper, self).__init__(*args, **kwargs)
        self.form_method = "POST"
        self.form_action = reverse('pledge-create')
        self.render_required_fields = True
        self.layout = Layout(
            Div(
                HTML("{% load i18n %}<span class='col-1 mr-2'>{% trans 'Montant&nbsp:' %}&nbsp;</span>"),
                InlineRadios('value_list',
                             template="value_list.html",
                             css_class="col-5 row no-gutters"),
                Div(
                    Field('value_text',
                          placeholder="80 €",
                          size=8),
                    css_class="col-3 value-text p-2",
                ),
                Field('recurring_field',
                      template="checkbox_slider.html",
                      css_class="col"),
                css_class="justify-content-center form-inline row my-4"),
            Div(
                Div('pseudo', css_class="col-4"),
                Div('email', css_class="col-4"),
                Div('password', css_class="col-4"),
                css_class="justify-content-center form-inline row my-4"),
            Div(
                Submit('submit',
                       _('Je donne&nbsp;!'),
                       css_class="col-4 btn-lg dark-blue-bg mx-auto mx-auto"),
                css_class="row my-2")
        )


class PledgeCreateForm(forms.ModelForm):
    """
    Form used to create a Pledge. We provide a systempay processor
    as default
    """
    class Meta:
        model = Pledge
        fields = ['pseudo', 'content_type', 'object_id']
        widgets = {'content_type': forms.HiddenInput(),
                   'object_id': forms.HiddenInput()}

    #: Email used to login/create an account and needed for recurring
    email = forms.EmailField(required=False,
                             label=_("Email"))

    #: Password used to login/create an account.
    password = forms.CharField(required=False,
                               label=_('Mot de passe'),
                               widget=forms.PasswordInput)

    #: Boolean used to detect if we're recurring or not.
    recurring_field = forms.BooleanField(required=False,
                                         label=_('Mensuel'))
    #: A radio list of prefilled amount
    value_list = forms.ChoiceField(
        label='',
        choices=[(10, '10 €'),
                 (30, '30 €'),
                 (50, '50 €'),
                 (100, '100 €'),
                 (250, '250 €'), ],
        widget=forms.RadioSelect(),
        required=False)

    #: A free text field
    value_text = forms.CharField(required=False,
                                 label=_("Ou&nbsp:&nbsp;&nbsp;"))

    def clean(self):
        cleaned_data = super(PledgeCreateForm, self).clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        value_text = cleaned_data.get('value_text')
        value_list = float(cleaned_data.get('value_list'))

        try:
            if value_text and float(value_text) > 0.0:
                self.instance.value = float(value_text)
            else:
                self.instance.value = value_list
        except ValueError:
            raise forms.ValidationError(
                "You must enter a numeric value."
            )

        if email and not password:
            if not self.request.user.is_authenticated():
                auth = authenticate(username=cleaned_data['pseudo'],
                                    password=password)
                if not auth:
                    # Need to check if the user exists first
                    try:
                        User.objects.get(username=cleaned_data['pseudo'])
                        raise forms.ValidationError(
                            "This pseudo already exists, and the password is wrong.\
                            You can ask for a password reset in the navbar above."
                        )
                    except:
                        raise forms.ValidationError(
                            "You added an email address but we need a password\
                            to authenticate you or to create you an account."
                        )

        if cleaned_data['recurring_field']:
            if not email:
                raise forms.ValidationError(
                    "We need an email address for recurring pledge"
                )

            self.instance.recurring = self.instance.value
            self.instance.value = 0.0

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(PledgeCreateForm, self).__init__(*args, **kwargs)
        self.helper = PledgeCreateFormHelper(self)


class PledgeDeleteForm(forms.ModelForm):
    """
    This form is used to delete a pledge
    """
    class Meta:
        model = Pledge
        fields = ['id']
        widget = {'id': forms.HiddenInput()}

    def clean(self):
        cleaned_data = super(PledgeDeleteForm, self).clean()
        pledge = self.instance

        if pledge.state != 'error':
            raise forms.ValidationError(
                'You can only delete pledge in error state'
            )
        return cleaned_data
