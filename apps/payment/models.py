from datetime import date, datetime

from model_utils.managers import InheritanceManager
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError

from project.settings import DEBUG


class PaymentProcessor(models.Model):
    """
    This is a class used to keep track of parameters of a PaymentProcessor
    It should be subclassed using proxy class to provide the needed
    functionnality.

    All proxy class should implement a set of method, according to the
    recurring value.
    """

    class Meta:
        verbose_name = _('Payment processor')
        verbose_name_plural = _('Payment processors')

    #: Name of the PaymentProcessor
    name = models.CharField(max_length=200)

    #: List of parameters needed for the payment processor to work
    configs = JSONField(blank=True, default=list())

    #: We're an inherited objects
    objects = InheritanceManager()

    def __str__(self):
        return 'Payment processor {}'.format(self.name)

    # Those method must be implemented by all Processor
    def _process(self, ppp):
        raise NotImplementedError

    # Those method must be available if recurring is False
    def _verify(self, ppp):
        raise NotImplementedError

    # Those method must be available if recurring is True
    def _recur(self, ppp):
        raise NotImplementedError

    def _cancel(self, ppp):
        raise NotImplementedError

    def _pre_expire(self, ppp):
        raise NotImplementedError

    def _expire(self, ppp):
        raise NotImplementedError

    def _renewal(self, ppp):
        raise NotImplementedError


class SystempayProcessor(PaymentProcessor):
    """
    This class is used to process Credit Card pledges through systempay system.
    """
    class Meta:
        verbose_name = _('Payment processor through systempay')
        verbose_name_plural = _('Payment processors through systempay')

    def save(self, *args, **kwargs):
        """
        We need to check if we have all the needed configurations options
        before saving the Processor.
        """
        if len(self.configs) == 0:
            # there's no config provided
            raise ValidationError(_('Missing configuration'), code='missing')

        if len(self.configs) > 2:
            # We should have one or two configuration only. TEST or PRODUCTION.
            raise ValidationError(
                _('Too many configuration, 2 maximum are allowed'),
                code='invalid')

        for config in self.configs:
            if 'mode' not in config:
                raise ValidationError(_('Missing a mode for a configuration'),
                                     code='missing')
            if config['mode'] not in ('TEST', 'PRODUCTION'):
                raise ValidationError(_('Invalid mode %(mode)s'),
                                      params={'mode': config['mode']},
                                      code='invalid')
            if 'certificate' not in config:
                raise ValidationError(_('Missing a certificate for config'),
                                      code='missing')
            if 'shop_id' not in config:
                raise ValidationError(_('Missing a site_id for config'),
                                      code='missing')
            if 'url' not in config:
                raise ValidationError(_('Missing a url for config'),
                                      code='missing')

        super(SystempayProcessor, self).save(*args, **kwargs)

    @property
    def config(self):
        if len(self.configs) == 1:
            return self.configs[0]
        # In debug mode, let's get the TEST config. There is only 2 configs at
        # most
        for config in self.configs:
            if config['mode'] == 'TEST' and DEBUG:
                return config
            if config['mode'] == 'PRODUCTION' and not DEBUG:
                return config

    def _process(self, pledge, **kwargs):
        """
        We basically called the remote URL, so there's nothing much to do here.
        """
        pledge.date_change = date.today()
        pledge.save()

    def _verify(self, pledge, **kwargs):
        """
        We will assert a lot of data and update the pledge accordingly. All our
        arguments are wrapped in kwargs as a vads_ information
        """
        # First we need to check if we are a recurring one or not
        if pledge.recurring <= 0:
            assert kwargs['vads_trans_status'] == 'AUTHORISED'
            pledge.value = int(kwargs['vads_amount']) / 100
            pledge.transaction = kwargs['vads_trans_id']
        else:
            assert kwargs['vads_identifier_status'] == 'CREATED'
            pledge.recurring = int(kwargs['vads_sub_amount']) / 100
            pledge.transaction = kwargs['vads_identifier']
        pledge.change_date = datetime.strptime(kwargs['vads_trans_date'],
                                              '%Y%m%d%H%M%S').date()
        pledge.save()

    def _recur(self, pledge, **kwargs):
        """
        Basically we want to check if we've not been already updated recently
        to avoid multiple incrementations.
        """
        date_transaction = kwargs.get('date_transaction')
        if date_transaction:
            date_transaction = datetime.strptime(date_transaction,
                                                 '%d/%m/%Y %X').date()
            assert pledge.date_change < date_transaction, "Last update of the pledge {} is after the transaction".format(pledge.pk)
            pledge.date_change = date_transaction
        pledge.value += pledge.recurring
        pledge.save()

    def _pre_expire(self, pledge, **kwargs):
        # Nothing to do here
        pass

    def _expire(self, pledge, **kwargs):
        # Basically nothing to do
        pledge.date_change = date.today()
        pledge.save()

    def _cancel(self, pledge, **kwargs):
        # Basically nothing to do
        pledge.date_change = date.today()
        pledge.save()


class WireTransferProcessor(PaymentProcessor):
    """
    This class is the simplest of all, and can be used for tests.
    It's a proxy for PaymentProcessor, implementing some methods to keep track
    of a wire payment.
    """
    class Meta:
        verbose_name = _('Payment processor for wire transfers')
        verbose_name_plural = _('payment processors for wire transfers')

    def _process(self, pledge):
        # We've got nothing to do here.
        pass

    def _verify(self, pledge, **kwargs):
        """
        This method is used to validate a pledge. If valid is False however
        it will set the pledge in error.
        """
        pledge.transaction = kwargs.get('transaction_id', None)
        pledge.save()
