import pytz
import hmac
import hashlib
import base64
from datetime import datetime, date, time
from uuid import uuid4

from suds.sax.element import Element


def get_local_timestamp(d, tz):
    """
    Convert a timestamp to a local one
    """
    tz = pytz.timezone(tz)
    if type(d) is date:
        d = datetime.combine(d, time.min)

    tz_aware = tz.localize(d).replace(microsecond=0)
    return tz_aware.strftime('%Y-%m-%dT%H:%M:%SZ')


def get_utc_timestamp(d):
    """
    Convert a timestamp to a UTC one.
    """
    return get_local_timestamp(d, 'UTC')


def create_systempay_headers(config):
    """
    This function takes a Processor configurations and creates a dictionary of
    fields to be added to the soapheades options of a suds.Client
    """
    namespace = ('soapHeader', 'http://v5.ws.vads.lyra.com/Header/')
    requestId = str(uuid4())
    timestamp = get_utc_timestamp(datetime.now())
    headers = []
    headers.append(Element('shopId',
                           ns=namespace).setText(config['shop_id']))
    headers.append(Element('timestamp',
                           ns=namespace).setText(timestamp))
    headers.append(Element('mode', ns=namespace).setText(config['mode']))
    headers.append(Element('requestId', ns=namespace).setText(requestId))

    authToken = requestId + timestamp
    hmac_256 = hmac.new(config['certificate'].encode(),
                        authToken.encode(),
                        digestmod=hashlib.sha256)

    b64 = base64.b64encode(hmac_256.digest()).decode()
    headers.append(Element('authToken', ns=namespace).setText(b64))
    return headers
