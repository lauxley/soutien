from random import randint
import os
import shutil
import tempfile
import subprocess

from django import forms
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.files import File
from django.core.files.storage import get_storage_class
from django.template.loader import get_template
from django_fsm import transition

from campaign.models import Item


class ClothingItem(Item):
    """
    This model is used to define clothe items which needs sizes.

    Available sizes are defined in settings
    """
    class Meta:
        verbose_name = _('Clothing item')
        verbose_name_plural = _('Clothing items')

    #: This is the size and cut attributes
    size = models.CharField(max_length=4, choices=settings.SIZES_CHOICES, null=True,
                            blank=True)

    @property
    def base_fields(self):
        base_fields = super(ClothingItem, self).base_fields
        base_fields['size'] = forms.ChoiceField(choices=settings.SIZES_CHOICES,
                                                required=False)
        return base_fields

    def __str__(self):
        if self.size is not None:
            return '{} size {}'.format(self.kind, self.size)
        return '{}'.format(self.kind)


class StandardItem(Item):
    """
    This model is used to define item which doesn't need a parameter to work.
    Basically it's a non-abstract version of Item.
    """
    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')


class Piplome(Item):
    """
    This model will be used for the piplomes. We need to assign a generated_file
    and a decimals position.
    """
    class Meta:
        verbose_name = _('Piplome')
        verbose_name_plural = _('Piplomes')

    #: The position of the first decimals of pi to send to the user.
    decimal = models.PositiveIntegerField(null=True, blank=True)

    #: The path to the field to be recovered
    file = models.FileField(upload_to='piplomes/', null=True, blank=True)

    def get_absolute_url(self):
        return self.file.url

    def __str__(self):
        return '{} #{}'.format(self.kind, self.decimal)

    def need_generation(self):
        return True

    def need_shipment(self):
        return False

    @transition(field='status',
                source=['new', 'error', 'generated'],
                target='generated',
                conditions=[need_generation],
                )
    def generate(self):
        """
        We need to generate a pdf for counterpart which needs it. We will use
        the template provided by the PDFCounterpart kind.

        The filename will be pseudo_pledgeId_decimal.pdf
        """
        # We want to find the pseudo to use
        try:
            pseudo = self.bonus.pledge.pseudo
        except AttributeError:
            pseudo = self.bonus.donor.user.username

        # We want to get a decimal only if we haven't one before
        if not self.decimal:
            # First, let's get an available thousand of pi decimals, between 10 and
            # 200 000.
            used_decimals = Piplome.objects.filter(
                decimal__isnull=False).values('decimal')

            self.decimal = randint(10, 200000)
            while self.decimal in used_decimals:
                self.decimal = randint(10, 200000)

        # We now have a unique decimals. We should grab the pi decimals from a
        # static file.
        statics = get_storage_class(settings.STATICFILES_STORAGE)()
        decimals = ''

        with statics.open('pi_decimals', 'rb') as pi:
            # Let's seek the first decimals we want However, we need to add 1
            # to be at the exact value (array starts at 0) and we want the
            # first digit of the thousand, so another + 1
            pi.seek(self.decimal * 1000 + 2)
            decimals = pi.read(1000).decode()

        decimals = [decimals[i:i + 100] for i in range(0, 1000, 100)]

        context = {'pseudo': pseudo,
                   'decimal': self.decimal,
                   'pi': decimals}

        # We need to get the template, which is a tex file, and to apply the
        # template.
        filename = '{}-{}-{}'.format(pseudo, self.id, self.decimal)
        template_name = os.path.join('counterparts',
                                     os.path.basename(self.kind.pdfcounterpart.template))
        tex = get_template(template_name).render(context)

        # We're now saving the texfile before it's processed by pdflatex
        tmp_dir = tempfile.mkdtemp(prefix='piplome-')
        with open('{}/{}.tex'.format(tmp_dir, filename), 'wb') as f:
            f.write(bytes(tex, 'utf-8'))

        del tex

        # We need to add static/tex and . to the env for Popen
        env = os.environ
        env['TEXINPUTS'] = ':'.join(['.',
                                     os.path.join(statics.location, 'tex'),
                                    ''])
        # And we now play with the pdflatex processes
        error = subprocess.Popen(
            ['pdflatex', '{}.tex'.format(filename)],
            cwd=tmp_dir,
            stdin=open(os.devnull, 'r'),
            stdout=open(tmp_dir + '/stdout', 'wb'),
            stderr=open(tmp_dir + '/stderr', 'wb'),
            env=env
        ).wait()

        if error:
            if settings.DEBUG:
                print("Error log at {}/{}.log".format(tmp_dir, filename))

        else:
            pdf = open("{}/{}.pdf".format(tmp_dir, filename), 'rb')
            self.file.save('{}.pdf'.format(filename),
                           File(pdf),
                           save=False)

            shutil.rmtree(tmp_dir)


class PhysicalPrintItem(Item):
    """
    This model is used for items that are physical prints of another item.
    For instance, piplomes.
    """
    class Meta:
        verbose_name = _('Printed piplome')
        verbose_name_plural = _('Printed piplomes')

    #: This is a link to the Item to be printed.
    original = models.ForeignKey(Piplome,
                                 on_delete=models.CASCADE,
                                 blank=True,
                                 null=True)

    @property
    def base_fields(self):
        base_fields = super(PhysicalPrintItem, self).base_fields
        base_fields['original'] = forms.ModelChoiceField(queryset=Piplome.objects.filter(bonus__donor=self.bonus.donor), required=False)
        return base_fields

    def need_shipment(self):
        if self.original:
            return True
        return False

    def __str__(self):
        return 'Printed Piplome'
