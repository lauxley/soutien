# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ClothingItem, StandardItem, PhysicalPrintItem, Piplome


@admin.register(ClothingItem)
class ClothingItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'kind', 'status', 'bonus', 'address', 'size')
    list_filter = ('kind', 'bonus', 'address')
    search_fields = ('bonus__donor__user__email',)


@admin.register(StandardItem)
class StandardItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'kind', 'status', 'bonus', 'address')
    list_filter = ('kind', 'bonus', 'address')
    search_fields = ('bonus__donor__user__email',)


@admin.register(PhysicalPrintItem)
class PhysicalPrintItemAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'kind',
        'status',
        'bonus',
        'address',
        'original',
    )
    list_filter = ('kind', 'bonus', 'address')
    search_fields = ('bonus__donor__user__email',)


@admin.register(Piplome)
class PiplomeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'kind',
        'status',
        'bonus',
        'address',
        'decimal',
        'file',
    )
    list_filter = ('kind', 'bonus', 'address')
    search_fields = ('bonus__donor__user__email',)
