from datetime import date
from dateutil import relativedelta

from django.views.generic.base import TemplateView
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.contenttypes.models import ContentType
from django.db.models import Sum

from faq.models import FAQItem
from user.models import Donor, Pledge
from payment.forms import PledgeCreateForm
from payment.models import SystempayProcessor

from .models import Campaign


class HomeView(TemplateView):

    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        site = get_current_site(self.request)
        context = super(HomeView, self).get_context_data(*args, **kwargs)

        context['site'] = site
        # Let's get the featured faq items first.
        context['featured_faq'] = FAQItem.objects.filter(featured=True)

        # Let's compute the needed indicators for the progress bar
        campaign = Campaign.objects.get(running=True)
        context['campaign'] = campaign

        # Those are the bonuses for the campaign
        context['bonuses'] = campaign.bonus_set.exclude(threshold=0).order_by('threshold')

        # First, let's get all the pledges in a valid state which have
        # been processed since the start_date
        pledges = Pledge.objects.filter(state__in=['cancelled',
                                                   'verified',
                                                   'recurring',
                                                   'will_expire',
                                                   ]).filter(date_change__gte=campaign.start_date)

        # Now, all reccuring pledges altered since the creation (if
        # they're not reccuring, their value is 0 so ...) which gives
        # us the one who have been processed one way or another since the
        # campaign started.
        recurring_pledges = Pledge.objects.filter(recurring__gt=0,
                                                  state__in=['recurring',
                                                             'processed',
                                                             'will_expire'])

        # This is the total of cash we got since the beginning.
        confirmed_total = sum([p.value_since_date(campaign.start_date) for p in pledges])

        # Now we need to get the provisionnal amount of pledges each
        # months.
        monthly_total = 0
        if recurring_pledges:
            monthly = recurring_pledges.aggregate(total=Sum('recurring'))
            monthly_total = monthly['total']

        # The budget is supposed to run on a full year. So, we need to
        # compute the number of month passed since the campaign started
        # and to substract this to 12, to get the number of remaining
        # month.
        months = 12 - relativedelta.relativedelta(campaign.start_date,
                                                  date.today()).months

        # We only keep 70% of the recurring detail, based on previous
        # experiences.
        provisional_total = monthly_total * months * 0.7

        context["confirmed"] = int((confirmed_total + provisional_total) * 100 / campaign.target)
        context["confirmed_total"] = int(confirmed_total + provisional_total)

        # We need to create the form for the payment system
        # First, let's see if we're authenticated
        initial = {}
        if self.request.user.is_authenticated:
            donor = Donor.objects.get(user=self.request.user)
            initial['email'] = donor.user.email
            initial['pseudo'] = donor.user.username

        # Now let's get the systempay processor we want
        processor = campaign.systempay_processor
        initial['content_type'] = ContentType.objects.get_for_model(
            SystempayProcessor, for_concrete_model=False)
        initial['object_id'] = processor.id
        initial['value_list'] = 30

        # We need a pledge_form
        pledge_form = PledgeCreateForm(initial=initial,
                                       request=self.request)

        # In case we're authenticated, there's no need to display the password field
        if self.request.user.is_authenticated():
            pledge_form.helper.layout[1].pop(2)  # Password field is here
            for div in pledge_form.helper.layout[1]:
                div.css_class="col-6"

        # We want to add how many days are left
        context['remains'] = campaign.end_date - date.today()
        context['pledge_form'] = pledge_form

        return context


class FaqView(TemplateView):

    template_name = 'faq.html'

    def get_context_data(self, *args, **kwargs):
        site = get_current_site(self.request)
        context = super(FaqView, self).get_context_data(*args, **kwargs)

        context['site'] = site
        # Let's get the featured faq items first.
        context['featured_faq'] = FAQItem.objects.filter(featured=True).order_by('pk')
        context['faqs'] = FAQItem.objects.filter(featured=False)

        # Let's get the campaign for opengraph related things
        campaign = Campaign.objects.get(running=True)
        context['campaign'] = campaign

        return context
