# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Campaign, Bonus, Counterpart, PDFCounterpart, ClaimedBonus, Item


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'start_date',
        'end_date',
        'target',
        'running',
        'content_type',
        'object_id',
    )
    list_filter = ('start_date', 'end_date', 'running', 'content_type')


@admin.register(Bonus)
class BonusAdmin(admin.ModelAdmin):
    list_display = ('id', 'campaign', 'name', 'threshold', 'icon')
    list_filter = ('campaign',)
    raw_id_fields = ('counterparts',)
    search_fields = ('name',)


@admin.register(Counterpart)
class CounterpartAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'kind',
        'description',
        'icon',
        'shippable',
        'item_kind',
    )
    list_filter = ('shippable', 'item_kind')


@admin.register(PDFCounterpart)
class PDFCounterpartAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'kind',
        'description',
        'icon',
        'shippable',
        'item_kind',
        'template',
    )
    list_filter = ('shippable', 'item_kind')


@admin.register(ClaimedBonus)
class ClaimedBonusAdmin(admin.ModelAdmin):
    list_display = ('id', 'bonus', 'donor', 'pledge')
    search_fields = ('donor__user__email',)



@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'kind', 'status', 'bonus', 'address', 'updated')
    list_filter = ('kind', 'status', 'updated')
    search_fields = ('donor__user__email',)
