# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-31 12:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0005_merge_20171030_1712'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='user.Address'),
        ),
    ]
