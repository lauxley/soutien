import os

from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django import forms
from django_fsm import FSMField, transition, TransitionNotAllowed
from model_utils.managers import InheritanceManager
from model_utils import FieldTracker

from project.settings import BASE_DIR
from user.models import Pledge, Donor, Address


class Campaign(models.Model):
    """
    This model is used to define campaign date and objectives.
    """
    class Meta:
        verbose_name = _('Campaign')
        verbose_name_plural = _('Campaigns')

    #: Start date of the campaign
    start_date = models.DateField()

    #: End of the official campaign
    end_date = models.DateField()

    #: Target amount of money to reqch before end_date
    target = models.IntegerField()

    #: If the campaign is still running in the background without emphasis
    running = models.BooleanField(default=False)

    #: We can use a picture as a background of the intro section
    image = models.FileField(null=True)

    #: We need a reference to one default SystemPay to make our lives easier
    content_type = models.ForeignKey(ContentType,
                                     limit_choices_to={
                                         'app_label': 'payment'},
                                     on_delete=models.SET_NULL,
                                     null=True)
    object_id = models.PositiveIntegerField(null=True)
    systempay_processor = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "{} - {} ({})".format(self.start_date,
                                     self.end_date,
                                     self.target)


class BonusManager(models.Manager):
    def get_elligible(self, recipient):
        """
        This function returns the elligible bonuses for a recipient
        """
        qs = super(BonusManager, self).get_queryset()
        if isinstance(recipient, Donor):
            available = recipient.cumul - recipient.claimed
            return qs.filter(Q(threshold__lte=available) & Q(threshold__gt=0))
        if isinstance(recipient, Pledge):
            cb = getattr(recipient, 'claimedbonus_set', None)
            if cb.count() > 0:
                return qs.none()
            return qs.filter(threshold__exact=0)


class Bonus(models.Model):
    """
    This model purpose is to group counterparts and add them to
    a campaign and a minimum threshold.

    If the treshold is 0, then the bonus can be claimed for each pledge
    made during the campaign. Otherwise it can be claim only once the user
    remaining cumulative pledge reach the threshold.
    """

    class Meta:
        verbose_name = _('Bonus')
        verbose_name_plural = _('Bonuses')

    #: To which campaign is this bonus active
    campaign = models.ForeignKey('Campaign',
                                 on_delete=models.CASCADE)

    #: Name of the bonus
    name = models.CharField(max_length=200)

    #: Threshold at which the bonus is available.
    threshold = models.IntegerField(default=0)

    #: We need an image to be added to bonus. Since we want SVG, it's a filefield
    icon = models.FileField(upload_to='uploads/', null=True, blank=True)

    #: Counterparts linked to the Bonus
    counterparts = models.ManyToManyField('Counterpart')

    #: We want to use the custom Manager
    objects = BonusManager()

    def __str__(self):
        return '{} - {} €'.format(
            self.name,
            self.threshold)


class Counterpart(models.Model):
    """
    This class is used to describe counterparts, generates them and associates
    them to a recipient (either a pledge or a user, according to the threshold
    of the bonus the counterparts belongs to.
    """

    class Meta:
        verbose_name = _('Counterpart')
        verbose_name_plural = _('Counterparts')

    #: Kind of the counterpart
    kind = models.CharField(max_length=200)

    #: Description of the counterpart
    description = models.TextField(max_length=1000, blank=True)

    #: Icon used to represent the counterpart.
    icon = models.FileField(upload_to='uploads/', null=True, blank=True)

    #: Determines if the generated counterpart must be shipped.
    shippable = models.BooleanField(default=True)

    #: Which kind of Item this counterpart will generate
    item_kind = models.ForeignKey(ContentType,
                                  limit_choices_to={'app_label': 'lqdn'},
                                  on_delete=models.CASCADE)

    def __str__(self):
        return self.kind


class PDFCounterpart(Counterpart):
    """
    The PDF counterpart is used to generate a pdf as a counterpart which can
    then be downloaded by the user.

    A PDF Counterpart can be a shippable Counterpart if needed.
    """

    class Meta:
        verbose_name = _('PDF Counterpart')
        verbose_name_plural = _('PDF Counterparts')

    #: Template used to generate the Counterpart
    template = models.FilePathField(
        path=os.path.join(BASE_DIR, 'apps/campaign/templates/counterparts/'),
        allow_files=True,
        recursive=True)


class ClaimedBonus(models.Model):
    """
    A claimed bonus is used to keep a track of who claimed a bonus
    """

    class Meta:
        verbose_name = _('Bonus claimed')
        verbose_name_plural = _('Bonuses claimed')

    #: The kind of Bonus being claimed, an instance of an existing Bonus
    bonus = models.ForeignKey(Bonus, on_delete=models.CASCADE)

    #: If the bonus is attached to a user, this is the user it belongs to
    donor = models.ForeignKey(Donor, on_delete=models.CASCADE, null=True)

    #: If the bonus is attached to a pledgem this is the pledge it belongs to
    pledge = models.ForeignKey(Pledge, on_delete=models.CASCADE, null=True)

    def __str__(self):
        if self.donor:
            return "{} for {}".format(self.bonus, self.donor)
        return "{} for {}".format(self.bonus, self.pledge)

    def clean(self):
        """
        Donor and Pledge cannot be set bot at the same time.
        """
        if self.donor is not None and self.pledge is not None:
            raise ValidationError({
                'donor': ValidationError(
                    _("Both donor and pledge are not None"),
                    code='invalid'),
                'pledge': ValidationError(
                    _("Both donor and pledge are not None"),
                    code='invalid'),
            })
        if self.donor is None and self.pledge is None:
            raise ValidationError({
                'donor': ValidationError(
                    _("Both donor and pledge are None"),
                    code='invalid'),
                'pledge': ValidationError(
                    _("Both donor and pledge are None"),
                    code='invalid'),
            })
        if self.donor is not None:
            # Are we elligible to this Bonus ?
            if self.bonus not in Bonus.objects.get_elligible(self.donor):
                raise ValidationError(
                    _("Donor {} is not elligible to {}".format(
                        self.donor, self.bonus)),
                    code="invalid")
        else:
            if self.bonus not in Bonus.objects.get_elligible(self.pledge):
                raise ValidationError(
                    _("Pledge #{} is not elligible to {}".format(
                        self.donor, self.bonus)),
                    code="invalid")

    def save(self, *args, **kwargs):
        """
        When a claimed bonus is created, we should instanciate the Items
        """
        super(ClaimedBonus, self).save(*args, **kwargs)

        for counterpart in self.bonus.counterparts.all():
            item = counterpart.item_kind.model_class()()
            if isinstance(item, Item):
                # We need to fill up some defaults
                # We need to keep track of the bonus we're using
                item.bonus = self
                item.kind = counterpart
                # We're going to generate items but they never
                # been saved before, so let's go for it
                item.save()
                item.refresh_from_db()
                try:
                    item.generate()
                    item.save()
                except TransitionNotAllowed:
                    item.save()
                except Exception as e:
                    item.error()
                    item.save()


class Item(models.Model):
    """
    This class is used to keep track of Items for a user, and in which state
    they are.
    """

    #: We're using the InheritanceManager now
    objects = InheritanceManager()

    #: The kind of counterpart the item is linked to.
    kind = models.ForeignKey(Counterpart,
                             on_delete=models.CASCADE)

    #: The state of the counterpart, it starts at new.
    status = FSMField(default='new')

    #: We want to keep an eye on the status of items
    status_tracker = FieldTracker(fields=['status', ])

    #: The ClaimedBonus responsible for the existence of this Item
    bonus = models.ForeignKey(ClaimedBonus,
                              related_name='item_set',
                              related_query_name='items',
                              on_delete=models.CASCADE)

    #: An address, needed to deliver the item.
    address = models.ForeignKey(Address, blank=True, null=True,
                                on_delete=models.SET_NULL)

    #: This is used to keep track of the last update done on an item
    updated = models.DateField(auto_now=True)

    #: We're going to use an InheritanceManager for all the items
    objects = InheritanceManager()

    #: This contains all the base fields to be added for every Item.form
    @property
    def base_fields(self):
        return {'item': forms.ModelChoiceField(queryset=Item.objects,
                                               widget=forms.HiddenInput,
                                               required=True),
                'app_label': forms.CharField(required=True,
                                             widget=forms.HiddenInput),
                'model': forms.CharField(required=True,
                                         widget=forms.HiddenInput),
                'claim': forms.BooleanField(required=False)}

    @property
    def form(self):
        """
        This property returns a BaseForm depending on state of the item and its
        property. It should be subclassed anywhere but it returns a class which
        means it can be used in formset.

        By default we want to display the following Fields with the following
        condition :
            - Address if we're in a claimed state AND the item is shippable
            - Hidden to just provide with a noop field if we're claimed but NOT
            shippable.
        """
        base_fields = self.base_fields.copy()

        form = type('ItemForm',
                    (forms.BaseForm,),
                    {'base_fields': base_fields})
        return form

    @property
    def string(self):
        return self.__str__()

    def __str__(self):
        return self.kind.kind

    def need_generation(self):
        try:
            if self.kind.pdfcounterpart:
                return False
            return True
        except:
            return True

    def not_need_generation(self):
        if self.need_generation():
            return False
        return True

    def need_shipment(self):
        """
        We can ship when an adress has been associated with us.
        """
        return self.kind.shippable

    @transition(field='status',
                source=['new', 'error'],
                target='claimed',
                conditions=[not_need_generation])
    def claim(self):
        """
        We should claim when all choices are made. Meaning all params have a
        ChosenValue. And we have an address.
        """
        pass

    @transition(field='status',
                source=['new', 'error', 'generated', ],
                target='generated',
                conditions=[need_generation])
    def generate(self):
        """
        We need to generate a pdf for counterpart which needs it.

        Subclasses should execute the parent at the end of their own code.
        """
        if self.need_shipment():
            self.prepare()

    @transition(field='status',
                source=['claimed'],
                target='ready',
                conditions=[need_shipment])
    def prepare(self):
        """
        Prepare an item to be shipped. It is mostly an administrative state
        used to determine if we must ship an item or not.
        """
        pass

    @transition(field='status',
                source=['ready'],
                target='done')
    def ship(self):
        """
        An item has been sent.
        """
        pass

    @transition(field='status',
                target='error')
    def error(self):
        """
        This item is in error
        """
        pass
