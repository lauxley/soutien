from modeltranslation.translator import translator, TranslationOption, register

from . import Bonus, Counterpart, Param


@register(Bonus)
class BonusTranslationOptions(TranslationOption):
    fields = ('name',)


@register(Counterpart)
class CounterpartTranslationOption(TranslationOption):
    fields = ('kind', 'description',)
