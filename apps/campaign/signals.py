from datetime import date
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.template.loader import get_template

from .models import Item


@receiver(pre_save)
def item_verified_email(sender, **kwargs):
    item = kwargs['instance']

    if isinstance(item, Item):
        # This is an item

        if not item.bonus.donor:
            # We have an item linked to a plegde, exit
            pass

        donor = item.bonus.donor

        if 'status' in item.status_tracker.changed() and item.status == 'done':
            dst = donor.user.email
            send_mail(
                'Thank you gifts from LQDN has been sent to you!',
                get_template('email/sent_counterparts.html').render(
                    {
                        'donor': item.bonus.donor,
                        'item': item,
                        'today': date.today(),
                    },
                ),
                'contact@laquadrature.net',
                [dst, ],)
