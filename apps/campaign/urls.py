from django.contrib.auth.decorators import login_required
from django.conf.urls import url

from .views import (HomeView, FaqView)

urlpatterns = [
    url(r'^$',
        HomeView.as_view(),
        name='campaign-home'
    ),
    url(r'^faq$',
        FaqView.as_view(),
        name='campaign-faq'
    ),
]
