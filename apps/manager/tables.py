import django_tables2 as tables

from campaign.models import Item
from user.models import Pledge


class ItemTable(tables.Table):

    #: This column is used to display the full details of the item (size, path)
    full_name = tables.Column(accessor='string')

    #: This is a checkbox used to act on items
    action = tables.CheckBoxColumn(accessor='pk',
                                   verbose_name='Action',
                                   exclude_from_export='True')

    #: Let's grab the donor name
    donor = tables.Column(accessor='bonus.donor.user.username')

    #: The donor email
    email = tables.Column(accessor='bonus.donor.user.email')

    class Meta:
        model = Item
        fields = ['id', 'donor', 'email', 'status', 'address', 'updated']


class PledgeTable(tables.Table):

    #: Let's grab the donor name
    donor = tables.Column(accessor='donor.user.username')

    #: The donor email
    email = tables.Column(accessor='donor.user.email')

    #: Let's add the at the end of value
    value = tables.Column(
        accessor='value',
        footer=lambda table: sum([x.value for x in table.data]))

    #: Let's add the recurring sum at the end of value
    recurring = tables.Column(
        accessor='recurring',
        footer=lambda table: sum([x.recurring for x in table.data]))

    def order_date_creation(self, queryset, is_descending):
        queryset = queryset.order_by(('-' if is_descending else '') + 'date_creation')
        return (queryset, True, )

    def order_date_change(self, queryset, is_descending):
        queryset = queryset.order_by(('-' if is_descending else '') + 'date_change')
        return (queryset, True, )

    class Meta:
        model = Pledge
        fields = ['id', 'state', 'value', 'recurring', 'pseudo', 'transaction'
                  'date_creation', 'date_change']
