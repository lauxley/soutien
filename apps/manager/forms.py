from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django import forms

from campaign.models import Item


class ItemFilterHelper(FormHelper):
    form_method = 'GET'
    layout = Layout('bonus__donor',
                    'kind',
                    'status',
                    'email',
                    Submit('submit', 'Filter'))


class ItemActionForm(forms.Form):
    status = forms.CharField()
    action = forms.ModelMultipleChoiceField(queryset=Item.objects.all(),
                                           widget=forms.CheckboxSelectMultiple)


class PledgeFilterHelper(FormHelper):
    form_method = 'GET'
    layout = Layout('recurring',
                    'state',
                    'date_creation__range',
                    'date_change__range',
                    'donor__name',
                    'donor__email',
                    Submit('submit', 'Filter'))


class CSVForm(forms.Form):
    csv = forms.FileField()
