import MySQLdb as mysql

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from user.models import Donor


class Command(BaseCommand):
    help = "Import Donors from a mysql databases"

    def add_arguments(self, parser):
        parser.add_argument('mysql_server',
                            nargs='?',
                            type=str,
                            help='Address of the mysql server to connect to')
        parser.add_argument('mysql_user',
                            nargs='?',
                            type=str,
                            help='Username used to connect to mysql_server')
        parser.add_argument('mysql_password',
                            nargs='?',
                            type=str,
                            help='Password of the mysql_user')
        parser.add_argument('mysql_database',
                            nargs='?',
                            type=str,
                            help="Mysql database name to connect to")
        parser.add_argument('--null',
                            action='store_true',
                            dest='null',
                            default=False,
                            help='Only import Null donors')

    def handle(self, *args, **options):
        db = None
        try:
            self.stdout.write("Connecting to MySQL database", ending='')
            db = mysql.connect(host=options['mysql_server'],
                               user=options['mysql_user'],
                               password=options['mysql_password'],
                               database=options['mysql_database'])
            self.stdout.write("[ " + self.style.SUCCESS('OK') + ' ]')
        except Exception as e:
            self.stdout.write("[ " + self.style.ERROR('KO') + ' ]')
            raise CommandError(e)

        # We're now connected, we need to get all the users
        c = db.cursor()
        try:
            self.stdout.write("Retrieving users from MySQL database...")
            c.execute("""SELECT email, pseudo FROM users""")
            users = c.fetchall()
            self.stdout.write("\t{} users found.".format(len(users)))
            i = 0
            for user in users:
                username = ''
                if i % 10 == 0:
                    self.stdout.write("\t{} donors created...".format(i))
                    username = user[1].replace(' ', '_')
                if user[1] and options['null']:
                    continue

                if not user[1]:
                    username = user[0]

                user_syst = None
                try:
                    user_syst = User.objects.get_by_natural_key(user[1])
                except Exception as e:
                    try:
                        password = User.objects.make_random_password()
                        user_syst = User.objects.create_user(username=username.replace(' ',
                                                                                       '_'),
                                                             email=user[0],
                                                             password=password)
                    except Exception as e:
                        # We had an issue creating this user. Print it and
                        # move.
                        self.stderr.write(self.style.ERROR("{}".format(e)) + "{}".format(user))
                        continue
                try:
                    donor = Donor.objects.get(user=user_syst)
                except:
                    donor = Donor(user=user_syst)
                    donor.save()
                    i += 1
            self.stdout.write("{} donors created. [ ".format(i) +
                              self.style.SUCCESS("OK") + " ]")
        except Exception as e:
            raise CommandError(e)
