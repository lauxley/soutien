import MySQLdb as mysql

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify

from campaign.models import Campaign, Bonus, ClaimedBonus, Item, Counterpart, PDFCounterpart
from user.models import Address, Donor

class Command(BaseCommand):
    help = "Import Adresses from a mysql databases"

    def add_arguments(self, parser):
        parser.add_argument('mysql_server',
                            nargs='?',
                            type=str,
                            help='Address of the mysql server to connect to')
        parser.add_argument('mysql_user',
                            nargs='?',
                            type=str,
                            help='Username used to connect to mysql_server')
        parser.add_argument('mysql_password',
                            nargs='?',
                            type=str,
                            help='Password of the mysql_user')
        parser.add_argument('mysql_database',
                            nargs='?',
                            type=str,
                            help="Mysql database name to connect to")
        parser.add_argument('--null',
                            action='store_true',
                            dest='null',
                            default=False,
                            help='Only import Null donors')

    def handle(self, *args, **options):
        db = None
        try:
            self.stdout.write("Connecting to MySQL database", ending='')
            db = mysql.connect(host=options['mysql_server'],
                               user=options['mysql_user'],
                               password=options['mysql_password'],
                               database=options['mysql_database'])
            self.stdout.write("[ " + self.style.SUCCESS('OK') + ' ]')
        except Exception as e:
            self.stdout.write("[ " + self.style.ERROR('KO') + ' ]')
            raise CommandError(e)

        # This is a mapping from original tailles to new sizes size[taille]
        # contains the correct value.
        size = settings.IMPORT_SIZE_MAP
        cmap = settings.IMPORT_COUNTERPARTS_MAP
        # We're now connected, we need to get all the adresses
        # Let's get the bonus
        bonus = Bonus.objects.filter(campaign__running=True).order_by('-threshold')
        c = db.cursor()
        item_total = 0
        try:
            # We're going to work incrementally by users and add the highest
            # bonus they have, and incrementing needed ones.
            # Everything else will be marking them as shipped
            donors = Donor.objects.all()
            i = 0
            self.stdout.write("Importing counterparts for {} Donors...".format(len(donors)))
            for donor in donors:
                if i % 10 == 0 and i > 0:
                    self.stdout.write("\t{} Donors parsed".format(i))

                addresses = donor.address_set.all()
                if not addresses:
                    i += 1
                    continue
                counterparts = c.execute("""SELECT quoi, taille, datec,
                                             contreparties.status, users.pseudo
                                         FROM contreparties
                                         JOIN users
                                            ON contreparties.user_id = users.id
                                         WHERE users.email = '{}'""".format(donor.user.email))
                counterparts = c.fetchall()
                # Lets' count stuff. This will give us the number of
                # ClaimedBonus to create.
                hoodies = [c for c in counterparts if c[0] == 'hoodie']
                pishirts = [c for c in counterparts if c[0] == 'pishirt']
                pibags = [c for c in counterparts if c[0] == 'pibag']
                piplomes= [c for c in counterparts if c[0] == 'piplome']

                if counterparts and counterparts[0][4] and options['null']:
                    continue

                counter = 0
                # First ClaimedBonus are the hoopies
                for hoodie in hoodies:
                    cb = ClaimedBonus(donor=donor,
                                      bonus=bonus[0])
                    cb.save()
                    counter += 1
                # Next are the pishirts. Since we've already claimed
                # some of them, we will only create those above the counter
                for pishirt in pishirts[counter:]:
                    cb = ClaimedBonus(donor=donor,
                                      bonus=bonus[1])
                    cb.save()
                    counter += 1
                # Following are the pibags.
                for pibag in pibags[counter:]:
                    cb = ClaimedBonus(donor=donor,
                                      bonus=bonus[2])
                    cb.save()
                    counter += 1
                # And finally all the piplomes
                for piplome in piplomes[counter:]:
                    cb = ClaimedBonus(donor=donor,
                                      bonus=bonus[3])
                    cb.save()
                    counter += 1

                # Now we're going to get through all the Donor Items.
                # Pick one of the correct kind with status at new and update it
                counter = 0
                ct_hoodie = Counterpart.objects.get(kind=cmap['hoodie'])
                items = Item.objects.filter(bonus__donor=donor,
                                            status='new',
                                            kind=ct_hoodie).select_subclasses()
                for hoodie in hoodies:
                    status = 'claimed'
                    address = None
                    if hoodie[3] == 2:
                        status = 'done'
                        if addresses:
                            address = addresses[0]
                        else:
                            continue
                    item = items[counter]
                    item.size = size[hoodie[1]]
                    item.status = status
                    item.address = address
                    item.save()
                    item_total += 1

                ct_pishirt = Counterpart.objects.get(kind=cmap['pishirt'])
                items = Item.objects.filter(bonus__donor=donor,
                                            status='new',
                                            kind=ct_pishirt).select_subclasses()
                for pishirt in pishirts:
                    status = 'claimed'
                    address = None
                    if pishirt[3] == 2:
                        status = 'done'
                        if addresses:
                            address = addresses[0]
                        else:
                            continue
                    item = items[counter]
                    item.size = size[pishirt[1]]
                    item.status = status
                    item.address = address
                    item.save()
                    item_total += 1

                ct_pibag = Counterpart.objects.get(kind=cmap['pibag'])
                items = Item.objects.filter(bonus__donor=donor,
                                            status='new',
                                            kind=ct_pibag).select_subclasses()
                for pibag in pibags:
                    status = 'claimed'
                    address = None
                    if pibag[3] == 2:
                        status = 'done'
                        if addresses:
                            address = addresses[0]
                        else:
                            continue
                    item = items[counter]
                    item.status = status
                    item.address = address
                    item.save()
                    item_total += 1

                ct_piplome = Counterpart.objects.get(kind=cmap['piplome'])
                items = Item.objects.filter(bonus__donor=donor,
                                            status='new',
                                            kind=ct_piplome).select_subclasses()
                for piplome in piplomes:
                    status = 'claimed'
                    address = None
                    if piplome[3] == 2:
                        status = 'done'
                        if addresses:
                            address = addresses[0]
                        else:
                            continue
                    item = items[counter]
                    item.status = status
                    item.address = address
                    item.save()
                    item_total += 1

                i += 1
            self.stdout.write("{} items created for {} Donors. [ ".format(item_total, i)
                              + self.style.SUCCESS("OK") + " ]")


        except Exception as e:
            self.stderr.write("{}".format(e))
            raise CommandError(e)
