from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count

from user.models import Address, Donor, Pledge
from campaign.models import ClaimedBonus

class Command(BaseCommand):
    help = "Squash users by removing doubles and merging their content into one common user"

    def handle(self, *args, **kwargs):
        # First, let's get the probelmatic emails
        emails = Donor.objects.values(
            'user__email').annotate(Count(
                'user__email')).filter(user__email__count__gt=1)
        self.stdout.write("There is {} Donors to squash".format(
            len(emails)))

        for email_item in emails:
            email = email_item['user__email']
            donors = Donor.objects.filter(user__email=email)
            # Now we need to find if one of them is associated to
            # a user with a last_login time set, in which case
            # it will default to it.
            default_donor = donors[0]
            logged_donor = donors.exclude(user__last_login__isnull=True)
            if logged_donor:
                default_donor = logged_donor.order_by('-user__last_login')[0]

            self.stdout.write("Merging {}".format(default_donor.user))
            # Now, let's move everythong to the defaut donor
            for donor in donors:
                if donor == default_donor:
                    continue
                Pledge.objects.filter(donor=donor).update(donor=default_donor)
                ClaimedBonus.objects.filter(donor=donor).update(donor=default_donor)
                Address.objects.filter(donor=donor).update(donor=default_donor)
                donor.user.delete()
                donor.delete()
