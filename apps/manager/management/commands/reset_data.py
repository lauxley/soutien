from django.core.management.base import BaseCommand, CommandError
from django.utils.six.moves import input

from user.models import Donor, Pledge, Address
from campaign.models import ClaimedBonus, Item

class Command(BaseCommand):
    help = "Prepare the databases for an import. Removing all donor, pledges, items, addresses and claimedbonuses created."

    def handle(self, *args, **kwargs):
        self.stdout.write(self.style.ERROR("You're about to flush the databases."))
        cont = input(self.style.ERROR("Are you sure you want to do that ? [y|N] "))

        # We need to confirm that we're sure
        if cont not in ['y', 'Y']:
            raise CommandError("Stopping due to user request")

        # We will process from the bottom to the top
        items = Item.objects.all()
        self.stdout.write("Deleting {} Item objects. ".format(len(items)),
                          ending='')
        delete = items.delete()
        self.stdout.write("[ " + self.style.SUCCESS("OK") + " ]")

        # Now let's remove the ClaimedBonuses
        cb = ClaimedBonus.objects.all()
        self.stdout.write("Deleting {} ClaimedBonuses objects. ".format(len(cb)),
                          ending='')
        delete = cb.delete()
        self.stdout.write("[ " + self.style.SUCCESS("OK") + " ]")

        # Let's remove the Adresses
        addresses = Address.objects.all()
        self.stdout.write("Deleting {} Adress objects. ".format(len(addresses)),
                          ending='')
        delete = addresses.delete()
        self.stdout.write("[ " + self.style.SUCCESS("OK") + " ]")

        # Get rid of the pledges
        pledges = Pledge.objects.all()
        self.stdout.write("Deleting {} Pledges objects. ".format(len(pledges)),
                          ending='')
        delete = pledges.delete()
        self.stdout.write("[ " + self.style.SUCCESS("OK") + " ]")

        # Get rid of the Donors, but preserve the users
        donors = Donor.objects.all()
        self.stdout.write("Deleting {} Donor objects. ".format(len(donors)),
                          ending='')
        delete = donors.delete()
        self.stdout.write("[ " + self.style.SUCCESS("OK") + " ]")
