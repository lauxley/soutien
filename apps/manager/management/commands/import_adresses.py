import MySQLdb as mysql

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.utils.text import slugify

from user.models import Address, Donor

class Command(BaseCommand):
    help = "Import Adresses from a mysql databases"

    def add_arguments(self, parser):
        parser.add_argument('mysql_server',
                            nargs='?',
                            type=str,
                            help='Address of the mysql server to connect to')
        parser.add_argument('mysql_user',
                            nargs='?',
                            type=str,
                            help='Username used to connect to mysql_server')
        parser.add_argument('mysql_password',
                            nargs='?',
                            type=str,
                            help='Password of the mysql_user')
        parser.add_argument('mysql_database',
                            nargs='?',
                            type=str,
                            help="Mysql database name to connect to")
        parser.add_argument('--null',
                            action='store_true',
                            dest='null',
                            default=False,
                            help='Only import Null donors')

    def handle(self, *args, **options):
        db = None
        try:
            self.stdout.write("Connecting to MySQL database", ending='')
            db = mysql.connect(host=options['mysql_server'],
                               user=options['mysql_user'],
                               password=options['mysql_password'],
                               database=options['mysql_database'])
            self.stdout.write("[ " + self.style.SUCCESS('OK') + ' ]')
        except Exception as e:
            self.stdout.write("[ " + self.style.ERROR('KO') + ' ]')
            raise CommandError(e)

        # We're now connected, we need to get all the adresses
        c = db.cursor()
        try:
            self.stdout.write("Retrieving adresses from MySQL database...")
            c.execute("""SELECT users.email, users.pseudo, adresses.*
                      FROM adresses JOIN users
                      ON adresses.user_id = users.id
                      WHERE adresses.alias = 'principale'""")
            adresses = c.fetchall()
            self.stdout.write("\t{} adresses found.".format(len(adresses)))
            i = 0
            for address in adresses:
                if i % 10 == 0:
                    self.stdout.write("\t{} adresses created...".format(i))

                if address[1] and options['null']:
                    continue
                try:
                    donor = Donor.objects.get(user__email=address[0])
                except:
                    continue

                address = Address(donor=donor,
                                  slug=slugify(' '.join([address[3],
                                                         address[7]])),
                                  streetname=address[3],
                                  address='\r'.join(address[4:5]),
                                  zipcode=address[6],
                                  city=address[7],
                                  state=address[8] if address[8] else '',
                                  country=address[9])
                address.save()
                i += 1
            self.stdout.write("{} adresses created. [ ".format(i) +
                              self.style.SUCCESS("OK") + " ]")
        except Exception as e:
            self.stderr.write("{}".format(type(e)))
            raise CommandError(e)
