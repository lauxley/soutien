import MySQLdb as mysql

from django.utils.timezone import now
from django.core.management.base import BaseCommand, CommandError
from django.contrib.contenttypes.models import ContentType

from user.models import Donor, Pledge
from campaign.models import Campaign


class Command(BaseCommand):
    help = "Import Donors from a mysql databases"

    def add_arguments(self, parser):
        parser.add_argument('mysql_server',
                            nargs='?',
                            type=str,
                            help='Address of the mysql server to connect to')
        parser.add_argument('mysql_user',
                            nargs='?',
                            type=str,
                            help='Username used to connect to mysql_server')
        parser.add_argument('mysql_password',
                            nargs='?',
                            type=str,
                            help='Password of the mysql_user')
        parser.add_argument('mysql_database',
                            nargs='?',
                            type=str,
                            help="Mysql database name to connect to")
        parser.add_argument('--recurring',
                            action='store_true',
                            dest='recurring',
                            default=False,
                            help='Only import recurring pledges')
        parser.add_argument('--null',
                            action='store_true',
                            dest='null',
                            default=False,
                            help='Only import Null donors')

    def handle(self, *args, **options):
        db = None
        try:
            self.stdout.write("Connecting to MySQL database", ending='')
            db = mysql.connect(host=options['mysql_server'],
                               user=options['mysql_user'],
                               password=options['mysql_password'],
                               database=options['mysql_database'])
            self.stdout.write("[ " + self.style.SUCCESS('OK') + ' ]')
        except Exception as e:
            self.stdout.write("[ " + self.style.ERROR('KO') + ' ]')
            raise CommandError(e)

        # We're now connected, we need to get all the users
        c = db.cursor()

        # We want to use the Campaign.processor_set as our default processor
        campaign = Campaign.objects.get(running=True)
        processor = campaign.systempay_processor

        try:
            if not options['recurring']:
                self.stdout.write("Importing non recurring pledges first...")
                self.stdout.write("Retrieving dons from MySQL database...")
                c.execute("""SELECT datec, somme, email, pseudo
                          FROM dons
                          JOIN users ON dons.user_id = users.id
                          WHERE dons.status=1""")

                pledges = c.fetchall()
                self.stdout.write("\t{} non recurring pledges found.".format(len(pledges)))
                i = 0
                for pledge in pledges:
                    if i % 100 == 0 and i != 0:
                        self.stdout.write("\t{} non recurring pledges created...".format(i))

                    if pledge[3] and options['null']:
                        continue

                    pseudo = pledge[3]
                    try:
                        donor = Donor.objects.get(user__email=pledge[2])
                        pseudo = donor.user.username
                    except:
                        donor = None
                    date_creation = pledge[0].date()
                    pledge_db = Pledge(value=pledge[1],
                                    recurring=False,
                                    pseudo=pseudo,
                                    state='verified',
                                    date_creation=date_creation,
                                    content_type=ContentType.objects.get_for_model(processor),
                                    object_id=processor.id,
                                    processor=processor)
                    if donor:
                        pledge_db.donor = donor
                    pledge_db.save()
                    pledge_db.refresh_from_db()
                    # We need to update the pledge to set date_creation and
                    # date_modification by hand
                    Pledge.objects.filter(pk=pledge_db.id).update(date_creation=date_creation,
                                                               date_change=date_creation)
                    i += 1
                    self.stdout.write("{} non recurring Pledges created. [ ".format(i) + self.style.SUCCESS("OK") + " ]")
            # Now we need to do some weirder shit for the recurring pledges. We
            # only want the 102 status, they're the one who had a recursion.
            today = now()
            c.execute("""SELECT min(datec), max(datec), somme, sum(somme),
                      email, pseudo, identifier
                      FROM dons
                      JOIN users ON dons.user_id = users.id
                      WHERE dons.status = 102
                      AND identifier IS NOT NULL
                      AND identifier != ''
                      GROUP BY identifier""")
            pledges = c.fetchall()
            self.stdout.write("\t{} recurring pledges found.".format(len(pledges)))
            i = 0
            for pledge in pledges:
                if i % 50 == 0 and i != 0:
                    self.stdout.write("\t{} recurring pledges created...".format(i))

                if pledge[5] and options['null']:
                    continue

                # We want to extract the date from datetime returnedby the
                # MySQL query
                date_creation = pledge[0].date()
                date_change = pledge[1].date()

                # We're recurring. The expiration of cards will be
                # done by managers
                status = 'recurring'

                # Forty days without an update is a reasonnable delay to
                # cancel a pledge
                delta = today.date() - date_change
                if delta.days > 40:
                    status = 'cancelled'

                # No donor means no game
                # FIXME: This is where things are weird for now
                donor = None
                try:
                    # First, let's try to get a user by its email address
                    donor = Donor.objects.get(user__email=pledge[4])
                except Donor.DoesNotExist:
                    # Next, we're trying to find a user with the same pseudo
                    try:
                        donor = Donor.objects.get(user__username=pledge[5].replace(' ',
                                                                                   '_'))
                    except Exception as e:
                        self.stdout.write(self.style.ERROR("No donor found: {}".format(pledge)))

                        # At this point we should have a Donor, meaning it have
                        # not been imported
                        continue
                except Donor.MultipleObjectsReturned as e:
                    # More than one donor returned, seems weird
                    donors = Donor.objects.filter(user__email=pledge[4])
                    self.stdout.write(self.style.ERROR(e))
                    self.stdout.write("{}".format(donors))
                    continue

                transaction = pledge[6]
                value = pledge[3]
                recurring = pledge[2]
                pseudo = donor.user.username

                try:
                    pledge = Pledge.objects.get(transaction=transaction)
                except:
                    pledge = Pledge(value=value,
                                    recurring=recurring,
                                    donor=donor,
                                    pseudo=pseudo,
                                    transaction=transaction,
                                    state=status,
                                    content_type=ContentType.objects.get_for_model(processor),
                                    object_id=processor.id,
                                    processor=processor)

                    pledge.save()
                    pledge.refresh_from_db()
                    # Let's update the dates
                    Pledge.objects.filter(pk=pledge.id).update(
                        date_creation=date_creation,
                        date_change=date_change)
                    i += 1
            self.stdout.write("{} recurring Pledges created. [ ".format(i) + self.style.SUCCESS("OK") + " ]")

        except Exception as e:
            self.stderr.write("{}".format(type(e)))
            raise CommandError(e)
