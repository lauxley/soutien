from django.conf.urls import url

from .views import (DashBoardView, ManageItemView, ManagePledgeView,
                    RecurringCSVView)

urlpatterns = [
    url(
        r'^$',
        DashBoardView.as_view(),
        name='manager-dashboard'
    ),
    url(
        r'^items/$',
        ManageItemView.as_view(),
        name='manager-item'
    ),
    url(
        r'^pledges/$',
        ManagePledgeView.as_view(),
        name='manager-pledge'
    ),
    url(
        r'^csv/recurring/$',
        RecurringCSVView.as_view(),
        name='manager-csv-recurring'
    )
]
