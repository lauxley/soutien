import csv
import re
from tempfile import TemporaryFile
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django_tables2.views import SingleTableView
from django_tables2.export.views import ExportMixin
from django_filters.views import FilterView
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import messages
from django.views.generic import TemplateView
from django.views.generic.edit import ProcessFormView
from django.db.models import Sum, Case, When, IntegerField, Count
from django.shortcuts import redirect, reverse

from campaign.models import Campaign, Item
from user.models import Pledge, Donor
from .filters import ItemFilter, PledgeFilter
from .tables import ItemTable, PledgeTable
from .forms import (ItemFilterHelper, ItemActionForm,
                    PledgeFilterHelper, CSVForm)


class DashBoardView(PermissionRequiredMixin, TemplateView):
    """
    This view is used as a dashboard to display some important information
    regarding the ongoing campaign and the demand of Items.

    Wewant to display the total amount of pledges for a campaign, plus the
    amount earned for each month for the las three months and the quantity of
    items waiting to be shipped.
    """

    template_name = 'dashboard.html'
    permission_required = 'user.is_manager'

    def get_context_data(self, **kwargs):
        context = super(DashBoardView, self).get_context_data(**kwargs)
        campaign = Campaign.objects.get(running=True)
        context['campaign'] = campaign

        # Pledges for the campaigna re all the pledge since the beginning of it
        pledges = Pledge.objects.filter(date_change__gt=campaign.start_date)
        today = date.today()
        first_of_month = today.replace(day=1)
        context['total'] = pledges.aggregate(Sum('value'))
        context['total_recurring'] = pledges.filter(recurring__gt=0).aggregate(Sum('recurring'))

        # We need to get the pledges value if recurring is 0 or reccuring if
        # not
        context['current_month'] = pledges.filter(
            date_change__gt=first_of_month).aggregate(total=Sum(
                Case(
                    When(recurring__gt=0, then='recurring'),
                    When(recurring=0, then='value'),
                    output_field=IntegerField(),)
            ))

        # Last month total
        last_month = first_of_month + relativedelta(months=-1)
        context['last_month'] = last_month
        context['last_month_pledges'] = pledges.filter(
            date_change__gt=last_month,
            date_change__lte=first_of_month).aggregate(total=Sum(
                Case(
                    When(recurring__gt=0, then='recurring'),
                    When(recurring=0, then='value'),
                    output_field=IntegerField(),)
            ))

        # An the month before that
        last_last_month = last_month + relativedelta(months=-2)
        context['last_last_month'] = last_last_month
        context['last_last_month_pledges'] = pledges.filter(
            date_change__gt=last_last_month,
            date_change__lte=last_month).aggregate(total=Sum(
                Case(
                    When(recurring__gt=0, then='recurring'),
                    When(recurring=0, then='value'),
                    output_field=IntegerField(),)
            ))

        # Let's get all the items
        items = Item.objects.all().filter(
            status='ready'
        ).values(
            'kind__kind'
        ).annotate(
            total=Count('kind')
        )
        context['items'] = items

        return context


class ManageItemView(PermissionRequiredMixin,
                     FilterView,
                     ExportMixin,
                     SingleTableView,
                     ProcessFormView):
    """
    This view display all the item, with filter and table view.

    We also need a checkbox to change the state of counterparts.
    """
    table_class = ItemTable
    model = Item
    filterset_class = ItemFilter
    template_name = 'manage_items.html'
    permission_required = 'user.is_manager'
    form = ItemActionForm

    def get_queryset(self, **kwargs):
        return Item.objects.all().order_by('-updated').select_subclasses()

    def get_context_data(self, **kwargs):
        context = super(ManageItemView, self).get_context_data(**kwargs)
        context['formhelper'] = ItemFilterHelper()
        return context

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        if form.is_valid():
            status = form.cleaned_data['status']
            for it in form.cleaned_data['action']:
                it = Item.objects.get_subclass(id=it.id)
                if status == 'generate':
                    try:
                        it.generate()
                    finally:
                        it.save()
                if status == 'ship':
                    try:
                        it.ship()
                    finally:
                        it.save()
        return redirect('manager-item', permanent=False)


class ManagePledgeView(PermissionRequiredMixin,
                       FilterView,
                       ExportMixin,
                       SingleTableView):
    """
    This view is used to display all the pledges, with filter and table view.

    There's a need to cancel pledges and to update them.
    """
    table_class = PledgeTable
    model = Pledge
    filterset_class = PledgeFilter
    template_name = 'manage_pledges.html'
    permission_required = 'user.is_manager'

    def get_queryset(self, **kwargs):
        return Pledge.objects.all().prefetch_related('donor')

    def get_context_data(self, **kwargs):
        context = super(ManagePledgeView, self).get_context_data(**kwargs)
        context['formhelper'] = PledgeFilterHelper()
        csv_recurring = CSVForm()
        csv_recurring.helper = FormHelper()
        csv_recurring.helper.form_action = reverse('manager-csv-recurring')
        csv_recurring.helper.form_method = 'POST'
        csv_recurring.helper.add_input(Submit('submit',
                                              'Update recurring pledges'))
        context['csv_recurring'] = csv_recurring
        return context


class RecurringCSVView(PermissionRequiredMixin,
                       ProcessFormView):
    form = CSVForm
    permission_required = 'user.is_manager'

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST, request.FILES)

        if form.is_valid():
            # This part is used to transform the CSV submitted in a temporary
            # file we can read
            upload = request.FILES['csv']
            temp_file = TemporaryFile('w+', encoding='utf-8')
            for chunk in upload.chunks():
                temp_file.write(chunk.decode('utf-8'))
            temp_file.seek(0)
            dialect = csv.unix_dialect
            dialect.delimiter = ';'
            reader = csv.DictReader(temp_file, dialect=dialect)

            # We'll need some lists of pledges
            updated_pledges = []
            nonexisting_pledges = []
            error_pledges = []
            expire_pledge = []

            for line in reader:
                # Let's get through all the pledges
                if line['Alias']:
                    pledge = None
                    try:
                        pledge = Pledge.objects.get(transaction=line['Alias'])
                    except Pledge.DoesNotExist as e:
                        # The pledge have not been found. We can probably try
                        # to do some educated guess and to find it anyway. See
                        # issue #138.
                        potentials = []

                        # First, we should check if it's an old one or not.
                        if re.match(r'\d{5}_.+', line['Alias']):
                            # We have a form NNNNN_aaaaaaa.... where the second
                            # part is the prefix of an email address. Which can
                            # be used to find Donors
                            email_prefix = line['Alias'].split('_', 1)[1]
                            # We can extract the creation_date to
                            date_creation = datetime.strptime(
                                line["Date de création"],
                                '%d/%m/%Y %H:%M:%S').date()
                            donors = Donor.objects.filter(user__email__startswith=email_prefix)
                            # Now, for each Donor, we need to find if they have
                            # a recurring pledge, with the same amount.
                            if not donors:
                                nonexisting_pledges.append(line['Alias'])
                                continue
                            potentials = Pledge.objects.exclude(
                                recurring=0
                            ).filter(
                                donor__in=donors,
                                date_creation=date_creation,
                                recurring=int(float(line['Montant du paiement']))
                            )
                        else:
                            # We have a new format pledge, much easier to
                            # identify
                            (date_creation, username) = line['Alias'].split('_', 1)
                            date_creation = datetime.strptime(date_creation,
                                                              '%Y%m%d%H%M%S').date()
                            potentials = Pledge.objects.exclude(recurring=0).filter(
                                date_creation=date_creation,
                                donor__user__username=username,
                                recurring=int(float(line['Montant du paiement']))
                            )

                        # We must see if there's only one potential pledge
                        if len(potentials) == 1:
                            # Hooray, only one pledge.
                            pledge = potentials[0]
                            pledge.transaction = line['Alias']
                        else:
                            nonexisting_pledges.append(line['Alias'])
                            continue

                    if line['Statut rapprochement'] == 'À analyser':
                        # The pledge is ok
                        if pledge.recurring == 0:
                            continue
                        date_expiration = datetime.strptime(
                            line["Date d'expiration"],
                            '%m/%Y').date()

                        try:
                            pledge.recurrence(date_transaction=line['Date remise'])
                        except AssertionError as e:
                            pledge.state = 'recurring'
                            error_pledges.append(pledge.pk)
                        finally:
                            relative = relativedelta(date_expiration, date.today())
                            if relative.months <= 1 and relative.years == 0:
                                # The credit card will expire before the next
                                # month
                                try:
                                    pledge.will_expires()
                                    if relative.months <= 0:
                                        pledge.expires()
                                except:
                                    pledge.error()
                                finally:
                                    expire_pledge.append((pledge.pk,
                                                          pledge.donor.user.email,
                                                          date_expiration,))

                            updated_pledges.append(pledge.pk)
                            pledge.save()
                    else:
                        if line['Statut rapprochement'] == 'En attente':
                            # Pledge is waiting
                            pledge.recurrence()
                            pledge.save()
                            updated_pledges.append(pledge.pk)
            if error_pledges:
                messages.error(request, "Pledges already updated: {}".format(error_pledges))
            if nonexisting_pledges:
                messages.error(request, 'Alias not found in databases, need to import them: {}'.format(nonexisting_pledges))
            messages.success(request, '{} Updated pledges'.format(len(updated_pledges)))
            if expire_pledge:
                messages.success(request, 'Credit card which will be cancelled before next month : {}'.format(expire_pledge))
            # Now we need to put all pledge not updated to cancelled state
            cancelled_pledges = Pledge.objects.exclude(pk__in=updated_pledges).exclude(state__in=['error',
                                                                                                  'cancelled']).exclude(recurring=0)
            for pledge in cancelled_pledges:
                try:
                    pledge.cancel()
                finally:
                    pledge.save()
            if cancelled_pledges:
                messages.success(request, '{} Cancelled pledges'.format(len(cancelled_pledges)))

        return redirect('manager-pledge', permanent=False)
