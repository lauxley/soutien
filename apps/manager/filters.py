import django_filters

from campaign.models import Item
from user.models import Pledge


class ItemFilter(django_filters.FilterSet):
    """
    This is used to filter items by kind, donor name, donor email or status.
    """
    STATUS_CHOICES = (
        ('new', 'New'),
        ('claimed', 'Claimed by the user, waiting for configuration on their side.'),
        ('ready', 'To be shipped'),
        ('done', 'Done'),
    )

    #: We want to use a Choice filter for status
    status = django_filters.ChoiceFilter(choices=STATUS_CHOICES)

    #: We want to search by user email
    email = django_filters.Filter(name='bonus__donor__user__email',
                                  label='Email')

    class Meta:
        model = Item
        fields = ['kind', 'status', 'email']


class PledgeFilter(django_filters.FilterSet):
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('processed', 'Processed'),
        ('verified', 'Verified'),
        ('recurring', 'Recurring'),
        ('will_expire', 'Will expire next month'),
        ('expired', 'Credit card is expired'),
        ('cancelled', 'Payment has been cancelled'),
        ('error', 'Error'),
    )

    #: We need to filter by state
    state = django_filters.ChoiceFilter(choices=STATUS_CHOICES)

    #: Filtering by creation date range
    date_creation__range = django_filters.DateFromToRangeFilter(
        name='date_creation',
        lookup_expr='range')

    # :Filtering by update date range
    date_change__range = django_filters.DateFromToRangeFilter(
        name='date_change',
        lookup_expr='range')

    #: Filtering by recurring state
    recurring = django_filters.ChoiceFilter(
        choices=(
            ('true', "Monthly payments"),
            ('false', "One time payments"),
        ),
        name='recurring',
        method='filter_recurring')

    #: Filtering by donor
    donor__name = django_filters.CharFilter(
        name='donor',
        label='Username',
        lookup_expr='user__username__icontains',
    )

    donor__email = django_filters.CharFilter(
        name='donor',
        label='Email',
        lookup_expr='user__email__icontains',
    )

    class Meta:
        model = Pledge
        fields = ['state',
                  'date_creation',
                  'date_change',
                  'recurring']

    def filter_recurring(self, queryset, name, value):
        """
        This is used to filter the recurring pledges. If we're called, it means
        the filter is checked and True, so we should exclude anything from the
        queryset which have a recurring value greater than 0
        """
        if value:
            if value == 'true':
                queryset = queryset.exclude(recurring=0)
            if value == 'false':
                queryset = queryset.exclude(recurring__gt=0)
        return queryset
