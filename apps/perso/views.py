from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        UserPassesTestMixin)
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.views.generic.edit import ProcessFormView
from django.views.generic.base import ContextMixin
from django.views.generic import TemplateView
from django.forms import inlineformset_factory
from django.shortcuts import redirect

from user.models import Donor, Pledge, Address
from campaign.models import Campaign, Bonus, Item, ClaimedBonus

from .forms import (ClaimBonusForm, ClaimItemFormHelper, AddressForm,
                    DeleteBonusForm, AddressFormHelper, AddressFormSingleHelper)


class DonorMixin(LoginRequiredMixin, UserPassesTestMixin):
    """
    This is a mixin used to check that a user exist and is a donor
    """

    login_url = 'login'

    def test_func(self):
        return not self.request.user.is_anonymous

    def get_object(self):
        if self.request.user.is_anonymous:
            return None
        try:
            return Donor.objects.get(user=self.request.user)
        except Donor.DoesNotExist:
            return None


class HomeView(DonorMixin, TemplateView):

    template_name = 'perso-home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        # Let's get some donor related data first
        donor = self.get_object()
        context['donor'] = donor
        context['cumul'] = donor.cumul
        context['available'] = donor.cumul - donor.claimed

        # Now we need bonus data. First we need to get the current campaign.
        campaign = Campaign.objects.get(running=True)
        elligible = Bonus.objects.get_elligible(donor)
        bonuses = Bonus.objects.filter(
            campaign=campaign).exclude(
                threshold=0).order_by('threshold')
        context['bonuses'] = bonuses

        # We need to find out what is the next bonus, and how many euros we
        # we need to reach it.
        next_bonus = next_total =0
        try:
            next_bonus = Bonus.objects.filter(threshold__gt=context['available'])[0]
            next_total = next_bonus.threshold - context['available']
        except IndexError:
            # We have no more bonuses for the pledgers
            next_bonus = None
            next_total = -1
        context['next_bonus'] = next_bonus
        context['next_total'] = next_total

        # The bonus form is now a list of item to generate
        bonuses_form = []
        for bonus in bonuses:
            initial = {'donor': donor,
                       'bonus': bonus,
                       'enabled': bonus in elligible}
            bonuses_form.append(ClaimBonusForm(initial=initial))

        context['bonuses_form'] = bonuses_form

        # Let's get all the claimed bonus, and build a formset for that
        claimed_bonuses = ClaimedBonus.objects.filter(donor=donor)
        claimed_bonuses_form = []
        for cb in claimed_bonuses:
            initial = {'donor': donor,
                       'claimedbonus': cb,
                       'enabled': cb.item_set.all().exclude(status='done')}
            claimed_bonuses_form.append(DeleteBonusForm(initial=initial))

        context['claimed_bonuses_form'] = claimed_bonuses_form

        context['active_home'] = True

        return context


class CounterpartsView(DonorMixin, TemplateView):

    template_name = 'counterparts.html'

    def get_context_data(self, **kwargs):
        context = super(CounterpartsView, self).get_context_data(**kwargs)

        # Let's get some donor related data first
        donor = self.get_object()
        context['donor'] = donor

        # Now we want all the created items associated to our donors
        items = Item.objects.filter(
            Q(bonus__donor=donor) |
            Q(bonus__pledge__donor=donor)).select_subclasses()

        items_new = items.filter(status='new').filter(kind__shippable=True)

        # For each available item, we will set up the form to configure it
        # and add a submit button to claim it.
        # Asking for shipment will be done in another page.
        claim_formset = []
        form_id = 0
        for item in items_new:
            initial = {'item': item,
                       'app_label': item._meta.app_label,
                       'model': item.__class__.__name__.lower()}
            form = item.form(initial=initial, prefix="item-{:d}".format(form_id))
            form_id += 1
            form.helper = ClaimItemFormHelper(base_fields=form.base_fields)
            form.helper.form_tag = False
            form.icon = item.kind.icon
            claim_formset.append(form)

        context['claim_formset'] = claim_formset

        # Let's get the adresses we have
        # If none are found, then a form will be displayed to add one.
        context['addresses'] = Address.objects.filter(donor=donor)
        address_form = AddressForm(initial={'donor': donor},
                                  prefix='address')
        address_form.helper = AddressFormSingleHelper()
        address_form.helper.form_tag = False
        address_form.helper.disable_csrf = True
        context['address_form'] = address_form

        context['active_counterparts'] = True
        return context


class PiplomesView(DonorMixin, TemplateView):

    template_name = 'piplomes.html'

    def get_context_data(self, **kwargs):
        context = super(PiplomesView, self).get_context_data(**kwargs)

        # Let's get some donor related data first
        donor = self.get_object()
        context['donor'] = donor
        context['pledges'] = Pledge.objects.filter(donor=donor)

        # We want all our pledges without a piplome already
        piplomes_form = []
        for pledge in context['pledges']:
            for bonus in Bonus.objects.get_elligible(pledge):
                initial = {'bonus': bonus,
                           'pledge': pledge,
                           'enabled': True}
                piplomes_form.append(ClaimBonusForm(initial=initial))

        context['piplomes_form'] = piplomes_form

        piplomes = Item.objects.filter(bonus__pledge__donor=donor,
                                       bonus__bonus__threshold=0,
                                       status__in=('generated', 'error',)).select_subclasses()
        context['piplomes'] = piplomes

        context['active_piplomes'] = True
        return context


class HistoryView(DonorMixin, TemplateView):

    template_name = 'history.html'

    def get_context_data(self, **kwargs):
        context = super(HistoryView, self).get_context_data(**kwargs)

        # Let's get some donor related data first
        donor = self.get_object()
        context['donor'] = donor
        pledges = Pledge.objects.filter(
            donor=donor)
        items = Item.objects.filter(
            Q(bonus__donor=donor) |
            Q(bonus__pledge__donor=donor)).filter(
                status__in=('generated',
                            'ready',
                            'done',)).select_subclasses()

        # We now have pledges and items events, we need to add them into
        # one list of events.
        events = []
        for pledge in pledges:
            event = {}
            event['type'] = 'pledge'
            event['date'] = pledge.date_change
            event['object'] = pledge
            events.append(event)

        for item in items:
            event = {}
            event['type'] = 'item'
            event['date'] = item.updated
            event['object'] = item
            events.append(event)

        context['events'] = sorted(events,
                                   key=lambda x: x['date'],
                                   reverse=True)

        context['active_history'] = True
        return context


class DetailView(DonorMixin, TemplateView):

    template_name = 'infos.html'

    def get_context_data(self):
        context = super(DetailView, self).get_context_data()

        # Let's get some donor related data first
        donor = self.get_object()
        context['donor'] = donor

        address_formset = inlineformset_factory(Donor, Address,
                                                form=AddressForm,
                                                extra=1)
        context['addresses'] = address_formset(instance=context['donor'])
        addresses_helper = AddressFormHelper()
        addresses_helper.form_tag = False
        context['addresses_helper'] = addresses_helper

        context['active_infos'] = True
        return context


class AddressView(DonorMixin, ProcessFormView, ContextMixin):

    form = AddressForm

    def get_context_data(self):
        context = super(AddressView, self).get_context_data()
        donor = self.get_object()
        context['donor'] = donor

        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()

        address_formset = inlineformset_factory(Donor, Address,
                                                form=AddressForm,
                                                extra=1)
        addresses = address_formset(self.request.POST,
                                    instance=context['donor'])

        if addresses.is_valid():
            addresses.save()

        return redirect('perso-infos', permanent=False)


class ClaimBonusView(DonorMixin, ProcessFormView, ContextMixin):

    form = ClaimBonusForm

    def get_context_data(self, *args, **kwargs):
        context = super(ClaimBonusView, self).get_context_data(*args, **kwargs)
        context['donor'] = self.get_object()

        return context

    def post(self, request, *args, **kwargs):
        bonus = ClaimBonusForm(self.request.POST)

        if bonus.is_valid():
            if bonus.cleaned_data['enabled']:
                bonus.save()

        return redirect('perso-home', permanent=False)


class DeleteBonusView(DonorMixin, ProcessFormView, ContextMixin):

    form = DeleteBonusForm

    def get_context_data(self, *args, **kwargs):
        context = super(DeleteBonusView, self).get_context_data(*args,
                                                                **kwargs)
        context['donor'] = self.get_object()

        return context

    def post(self, request, *args, **kwargs):
        bonus = DeleteBonusForm(self.request.POST)

        if bonus.is_valid():
            if not bonus.cleaned_data['enabled']:
                bonus.cleaned_data['claimedbonus'].delete()

        return redirect('perso-home', permanent=False)


class ClaimItemView(DonorMixin, ProcessFormView, ContextMixin):

    def get_context_data(self):
        context = super(ClaimItemView, self).get_context_data()
        context['donor'] = self.get_object()

        return context

    def post(self, request, *args, **kwargs):
        # We want to get the address first
        address = None
        if 'address-select' in self.request.POST:
            address = Address.objects.get(pk=self.request.POST['address-select'])
        else:
            address_form = AddressForm(self.request.POST,
                                      prefix='address')
            if address_form.is_valid():
                address = address_form.save()
            else:
                return redirect('perso-counterparts', permanent=False)

        form_max = len(self.request.POST.getlist('csrfmiddlewaretoken'))
        form_id = 0
        while form_id < form_max:
            # We have a generic form, which will allow us to get
            # the correct ContentType, and then to get the real form.
            prefix = 'item-{}'.format(form_id)
            model = self.request.POST[prefix + '-model']
            app_label = self.request.POST[prefix + '-app_label']
            pk = self.request.POST[prefix + '-item']

            ct = ContentType.objects.get(model=model, app_label=app_label)
            item = ct.get_object_for_this_type(pk=pk)
            form = item.form(self.request.POST, prefix=prefix)
            form_id += 1

            if form.is_valid():
                if form.cleaned_data['claim']:
                    del form.cleaned_data['claim']
                    try:
                        for field in form.cleaned_data:
                            setattr(item, field, form.cleaned_data[field])
                        item.claim()
                        item.address = address
                        item.prepare()
                    finally:
                        item.save()

        return redirect('perso-counterparts', permanent=False)


class PrepareItemView(DonorMixin, ProcessFormView, ContextMixin):

    form = Item.form

    def get_context_data(self):
        context = super(PrepareItemView, self).get_context_data()
        context['donor'] = self.get_object()

        return context

    def post(self, request, *args, **kwargs):
        # We need to extract the Contenttype and the item to update the form on
        # the fly.
        ct = ContentType.objects.get(app_label=self.request.POST['app_label'],
                                    model=self.request.POST['model'])
        item = ct.get_object_for_this_type(pk=self.request.POST['pk'])

        form = item.form(self.request.POST)
        if form.is_valid():
            form.cleaned_data.pop('app_label')
            form.cleaned_data.pop('model')
            for key, value in form.cleaned_data.items():
                setattr(item, key, value)
            item.prepare()
            item.save()

        return redirect('perso-home', permanent=False)


class PledgeView(DonorMixin, TemplateView):
    pass
