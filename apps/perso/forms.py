from django import forms
from django.utils.translation import ugettext as _
from django.contrib.contenttypes.models import ContentType

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML, Fieldset, Submit, Field, Div

from campaign.models import ClaimedBonus, Bonus, Item
from user.models import Donor, Address, Pledge
from payment.forms import SystempayFormMixin


class ClaimItemFormHelper(FormHelper):
    """
    This formset is used to display a form to claim Items
    """
    def __init__(self, base_fields=[], *args, **kwargs):
        super(ClaimItemFormHelper, self).__init__(*args, **kwargs)
        self.form_action = 'item-claim'
        self.render_required_fields = True
        del base_fields['claim']
        self.layout = Layout(
            Field('claim', template="claim-item.html"),
            Div(
                Div(
                    HTML(
                     '<img src="{{ item.icon.url }}">'
                    ),
                    css_class="col-5"),
                Div(
                    Fieldset('{{ item.initial.item }}', *base_fields),
                    css_class="col-7"),
                css_class="selected-div m-1 h-100 row",
            ),
            HTML('</label>'),
        )


class ClaimBonusHelper(FormHelper):
    """
    This formset is used to display a bonus claim form with an image
    submit button.
    """
    def __init__(self, *args, **kwargs):
        super(ClaimBonusHelper, self).__init__(*args, **kwargs)
        self.form_action = 'perso-claim'
        self.layout = Layout(
            'bonus',
            'donor',
            'pledge',
            'enabled',
            HTML('<div class="text-center">'
                 '<input type="image" id="bonus-{{ bonus.initial.bonus.id}}" '
                 'src="{{ bonus.initial.bonus.icon.url }}" '
                 '{% if not bonus.initial.enabled %}disabled{% endif %}'
                 '/>'
                 '<label for="bonus-{{ bonus.initial.bonus.id }}">'
                 '{% for counterpart in bonus.initial.bonus.counterparts.all %}'
                 '{{ counterpart.kind }}'
                 '{% if not forloop.last %},&nbsp;{% endif %}'
                 '{% endfor %}'
                 '&nbsp;-&nbsp;{{ bonus.initial.bonus.threshold }}&nbsp;&euro;</label>'
                 '</div>'),
        )


class ClaimBonusForm(forms.ModelForm):
    """
    This form is used to populate formsets to claim bonuses
    on the donor view.
    """
    class Meta:
        model = ClaimedBonus
        fields = ['donor', 'bonus', 'pledge']

    #: The pledge field is needed but will be hidden
    pledge = forms.ModelChoiceField(required=False,
                                    widget=forms.HiddenInput(),
                                    queryset=Pledge.objects.all())

    #: The donor field will be defined by the view
    donor = forms.ModelChoiceField(required=False,
                                   widget=forms.HiddenInput(),
                                   queryset=Donor.objects.all())

    #: The bonus list will be fed by the fieldset
    bonus = forms.ModelChoiceField(required=True,
                                   widget=forms.HiddenInput(),
                                   queryset=Bonus.objects.all())

    #: This allow to enable or disable a bonus
    enabled = forms.BooleanField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(ClaimBonusForm, self).__init__(*args, **kwargs)
        self.helper = ClaimBonusHelper()


class DeleteBonusHelper(FormHelper):
    """
    This form is used to display the conus claimed by the user to allow for
    deletion.

    It needs a bonus object available in the context.
    """

    def __init__(self, *args, **kwargs):
        super(DeleteBonusHelper, self).__init__(*args, **kwargs)
        self.form_action = 'perso-unclaim'
        self.layout = Layout(
            'claimedbonus',
            'donor',
            HTML('{% load i18n %}'
                 '<div class="text-left">'
                 '<input type="image" id="cb-{{ bonus.initial.claimedbonus.id }}"'
                 'src="{{ bonus.initial.claimedbonus.bonus.icon.url }}" '
                 '{% if not bonus.initial.enabled %}disabled{% endif %}'
                 '/>'
                 '<label for="cb-{{ bonus.initial.claimedbonus.id }}">'
                 '{% for counterpart in bonus.initial.claimedbonus.item_set.all %}'
                 '{% if counterpart.status == "done" %}'
                 '{{ counterpart.kind }} {% trans "Already shipped." %}'
                 '{% endif %}'
                 '{% endfor %}'
                 '{% for counterpart in bonus.initial.claimedbonus.item_set.all %}'
                 '{{ counterpart }}{% if not forloop.last %}&nbsp;,{% endif %}'
                 '{% endfor %}'
                 '</label>'
                 '</div>'),
        )


class DeleteBonusForm(forms.Form):
    """
    This form is used to delete a bonus
    """

    #: The donor field is needed to allow for removal of only owned bonuses
    donor = forms.ModelChoiceField(required=True,
                                   widget=forms.HiddenInput(),
                                   queryset=Donor.objects.all())

    #: The pk field is needed to identify the claimed bonus we're removing.
    claimedbonus = forms.ModelChoiceField(required=True,
                                          widget=forms.HiddenInput(),
                                          queryset=ClaimedBonus.objects.all())

    #: This allow to enable or disable a bonus
    enabled = forms.BooleanField(widget=forms.HiddenInput(),
                                 required=False)

    def __init__(self, *args, **kwargs):
        super(DeleteBonusForm, self).__init__(*args, **kwargs)
        self.helper = DeleteBonusHelper()


class AddressForm(forms.ModelForm):
    """
    This form will be used to ask for adresses input
    """
    class Meta:
        model = Address
        fields = ['donor',
                  'streetname',
                  'zipcode',
                  'address',
                  'city',
                  'state',
                  'country']

    #: The donor field is hidden
    donor = forms.ModelChoiceField(required=True,
                                   widget=forms.HiddenInput(),
                                   queryset=Donor.objects)

    #: The street name field is a bit ill named
    streetname = forms.CharField(required=True,
                                 label=_('Name'),
                                 help_text=_('Name on the mailbox'))


class AddressFormSingleHelper(FormHelper):
    """
    This formset is used to display a form for a single address input.
    """
    def __init__(self, *args, **kwargs):
        super(AddressFormSingleHelper, self).__init__(*args, **kwargs)
        self.layout = Layout(
            Div(
                Fieldset(
                    _('Address'),
                    'streetname',
                    'address',
                    css_class="col-sm-6"),
                Div(
                    Fieldset(_('City'), 'zipcode', 'city',
                             css_class='col-12'),
                    Div(css_class="w-100"),
                    Fieldset(_('Country'), 'state', 'country',
                            css_class='col-12'),
                    css_class="col-sm-6 row"),
                css_class="row"
            )
        )

class AddressFormHelper(FormHelper):
    """
    This formset is used to display all the adresses for a donor,
    To allow to update them all at once, or to add a new one.
    """
    def __init__(self, *args, **kwargs):
        super(AddressFormHelper, self).__init__(*args, **kwargs)
        self.form_action = 'perso-addresses'
        self.layout = Layout(
            Field('DELETE', css_class="form-control",
                 template="address_delete.html"),
            Div(
                Div(
                    Field('streetname', css_class="form-control"),
                    css_class="col"),
                Div(css_class="w-100"),
                Div(
                    Field('address', rows=3, css_class="form-control"),
                    css_class="col"),
                css_class="form-row"),
            Div(
                Div(
                    Field('zipcode', css_class="form-control"),
                    css_class="col"),
                Div(
                    Field('city', css_class="form-control"),
                    css_class="col"),
                css_class="form-row"),
            Div(
                Div(
                    Field('state', css_class="form-control"),
                    css_class="col"),
                Div(
                    Field('country', css_class="form-control"),
                    css_class="col"),
                css_class="form-row"),
            HTML("</label>"),
        )
        self.render_required_fields = True


class ShipItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = '__all__'


class ShipItemFormHelper(FormHelper):
    """
    This Helper is used to prepare shipment for Items.
    """
    def __init__(self, *args, **kwargs):
        super(ShipItemFormHelper, self).__init__(*args, **kwargs)
        self.form_action = 'item-prepare'
        self.render_required_fields = True


class UpdateClaimForm(SystempayFormMixin):
    vads_action_mode = forms.CharField(initial='INTERACTIVE',
                                      max_length=20,
                                      widget=forms.HiddenInput)
    vads_ctx_mode = forms.CharField(max_length=10,
                                    widget=forms.HiddenInput)
    vads_cust_email = forms.EmailField(widget=forms.HiddenInput)
    vads_identifier = forms.CharField(max_length=80,
                                      widget=forms.HiddenInput)
    vads_page_action = forms.CharField(initial='REGISTER_UPDATE',
                                       max_length=20,
                                       widget=forms.HiddenInput)
    vads_redirect_error_timeout = forms.IntegerField(initial=5,
                                                     widget=forms.HiddenInput)
    vads_redirect_success_timeout = forms.IntegerField(initial=5,
                                                       widget=forms.HiddenInput)
    vads_return_mode = forms.CharField(max_length=5,
                                       initial='GET',
                                       widget=forms.HiddenInput)
    vads_shop_name = forms.CharField(max_length=100,
                                     widget=forms.HiddenInput)
    vads_shop_url = forms.URLField(widget=forms.HiddenInput)
    vads_site_id = forms.CharField(max_length=20,
                                   widget=forms.HiddenInput)
    vads_trans_date = forms.CharField(max_length=20,
                                      widget=forms.HiddenInput)
    vads_url_check = forms.URLField(widget=forms.HiddenInput)
    vads_url_return = forms.URLField(widget=forms.HiddenInput)
    vads_version = forms.CharField(initial='V2',
                                   max_length=5,
                                   widget=forms.HiddenInput)

    signature = forms.CharField(max_length=64,
                                widget=forms.HiddenInput)
