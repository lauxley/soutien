from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequired, UserPassesTestMixin

from .models import Donor, Pledge


class DonorPledgeMixin(ListView, LoginRequired):
    model = Pledge

    def get_queryset(self):
        qs = super(DonorPledgeMixin, self).get_queryset()
        return qs.filter(donor__user=self.query.user)


class DonorDetailView(DetailView, LoginRequired, UserPassesTestMixin):
    model = Donor
    pk_url_kwarg = 'user_id'

    def test_func(self):
        return self.object.user == self.request.user
