from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.template.loader import get_template

from .models import Pledge


@receiver(pre_save, sender=Pledge)
def pledge_verified_email(sender, **kwargs):
    pledge = kwargs['instance']

    if not pledge.donor:
        pass

    if 'state' in pledge.state_tracker.changed() and pledge.state == 'verified':
        if pledge.state_tracker.previous('state') == 'processed':
            dst = pledge.donor.user.email
            send_mail(
                'Thanks for your support! / Merci de votre soutien !',
                get_template('email/thanks_support.html').render(
                    {
                        'donor': pledge.donor,
                        'pledge': pledge
                    },
                ),
                'contact@laquadrature.net',
                [dst, ],)
