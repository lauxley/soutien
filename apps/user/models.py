from dateutil import relativedelta
from datetime import date

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from django_fsm import FSMField, transition
from model_utils import FieldTracker


class Donor(models.Model):
    """
    Those are the donors who wants to have an account, to be able to claim
    things.
    """

    class Meta:
        verbose_name = _('Donor')
        verbose_name_plural = _('Donors')
        permissions = (
            ("is_manager", "Can perform management tasks",),
        )

    #: User account associated to this donor
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

    @property
    def cumul(self):
        """
        This property defines the value a user has given to LQDN since the
        beginning of times.
        """
        cumul = 0
        for pledge in self.pledges.exclude(state__in=['created',
                                                      'error',
                                                      'processed']):
            cumul += pledge.value
        return cumul

    @property
    def claimed(self):
        """
        This property computes the values of all claimed bonus for the users.
        Accessing pledge linked bonus is uselss, for they must have a threshold
        of 0.
        """
        claimed = 0
        for claimed_bonus in self.claimedbonus_set.all():
            claimed += claimed_bonus.bonus.threshold
        return claimed


class Pledge(models.Model):
    """
    This is a virtual class needed for keeping track of pledges. All the
    smartness and code is done in PaymentProcessor
    """
    STATUS_CHOICES = (
        ('created', 'Created'),
        ('processed', 'Processed'),
        ('verified', 'Verified'),
        ('recurring', 'Recurring'),
        ('will_expire', 'Will expire next month'),
        ('expired', 'Credit card is expired'),
        ('cancelled', 'Payment has been cancelled'),
        ('error', 'Error'),
    )

    class Meta:
        verbose_name = _('Pledge')
        verbose_name_plural = _('Pledges')\


    #: The value of the pledge
    value = models.FloatField()

    #: If the pledge is reccurent, this value is the recurring amount.
    #: If it's not, it is 0
    recurring = models.IntegerField(default=0)

    #: A pseudonym gave by the donor, mainly used to personalize bonuses
    pseudo = models.CharField(max_length=200)

    #: State of the pledge
    state = FSMField(default='created', choices=STATUS_CHOICES)

    #: We want to track the state
    state_tracker = FieldTracker(fields=['state'])

    #: Date at which the pledge is created
    date_creation = models.DateField(auto_now_add=True)

    #: Date at which the pledge has change
    date_change = models.DateField(default=date.today)

    #: The hypothetical donor linked to this pledge
    donor = models.ForeignKey(Donor, on_delete=models.DO_NOTHING, null=True,
                              related_name='pledges')

    #: Transaction id used to find the pledge in the ledger
    transaction = models.CharField(max_length=200, null=True, blank=True)

    #: PaymentProcessor used to process the pledge, using ContentType
    content_type = models.ForeignKey(ContentType,
                                     on_delete=models.CASCADE,
                                     limit_choices_to={
                                         'app_label': 'payment'
                                     },
                                    null=True)
    object_id = models.PositiveIntegerField(null=True)
    processor = GenericForeignKey('content_type',
                                  'object_id',
                                  for_concrete_model=False,
                                  )

    def __str__(self):
        if self.recurring > 0:
            return 'Monthly pledge of {} by {} since {}'.format(self.recurring,
                    self.pseudo, self.date_creation)
        return 'Pledge of {} by {} at {}'.format(self.value,
                self.pseudo, self.date_creation)

    def value_since_date(self, date):
        """
        This method is used to get the value from a pledge since a
        specific point in time.
        """
        if self.date_creation >= date:
            return self.value
        else:
            if self.recurring == 0:
                # We're not a recurring pledge and the pledge is older
                # than the date asked for
                return 0
            else:
                # We need to get an interval from the date, in month, and
                # to multiply it by the recurring amount.
                months = relativedelta.relativedelta(date,
                                                     self.date_creation).months
                return self.recurring * months

    @property
    def elligible(self):
        bonus = ContentType.objects.get(app_label='campaign', model='bonus')
        bonus = bonus.model_class()
        return bonus.objects.get_elligible(self)

    def is_recurring(self):
        return self.recurring > 0

    @transition(field='state',
                source='*',
                target='error')
    def error(self):
        pass

    @transition(field='state',
                source=['error', 'created'],
                target='processed')
    def process(self, processor):
        """
        This transition is merely used to link a pledge to a processor, and
        initialise the processor if needed.
        """
        self.processor = processor
        self.processor._process(self)

    @transition(field="state",
                source=['error', 'processed'],
                target='verified')
    def verify(self, **kwargs):
        self.processor._verify(self, **kwargs)

    @transition(field='state',
                source=['verified', 'recurring', 'will_expire', 'error',
                        'cancelled', 'processed'],
                target='recurring',
                conditions=[is_recurring])
    def recurrence(self, **kwargs):
        self.processor._recur(self, **kwargs)

    @transition(field='state',
                source='*',
                target='cancelled',
                conditions=[is_recurring])
    def cancel(self, **kwargs):
        self.processor._cancel(self, **kwargs)

    @transition(field='state',
                source=['error', 'recurring'],
                target='will_expire',
                conditions=[is_recurring])
    def will_expires(self, **kwargs):
        self.processor._pre_expire(self, **kwargs)


class Address(models.Model):

    """
    Those are adresses to which items that should be sent are sent.
    There's a possibility to have more than one address, because people can
    move or want to send Items to someone else than them
    """

    class Meta:
        verbose_name = _('Street address')
        verbose_name_plural = _('Street addresses')

    #: A slug used to identify the address
    slug = models.SlugField(allow_unicode=True)

    #: The donor to which the address is linked
    donor = models.ForeignKey(Donor, on_delete=models.CASCADE)

    #: The name on the mailbox to be used as a delivery
    streetname = models.CharField(max_length=200)

    #: Full adress, can be on more than ome line
    address = models.TextField(max_length=2000)

    #: ZipCode can be absolutely anything.
    zipcode = models.CharField(max_length=20)

    #: City
    city = models.CharField(max_length=200)

    #: State or Province and might be blank.
    state = models.CharField(max_length=200, blank=True, default='')

    #: Country
    country = models.CharField(max_length=200)

    def __str__(self):
        return '{}\r{}\r{} {}\r{} {}'.format(
            self.streetname,
            self.address,
            self.zipcode, self.city,
            self.state, self.country)
