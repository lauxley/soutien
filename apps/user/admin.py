# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Donor, Pledge, Address


@admin.register(Donor)
class DonorAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')
    search_fields = ('user__email',)


@admin.register(Pledge)
class PledgeAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'value',
                    'recurring',
                    'pseudo',
                    'state',
                    'date_creation',
                    'date_change',
                    'transaction',
                    'donor')
    list_filter = ('date_creation', 'state', 'donor__user__email')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'slug',
        'donor',
        'streetname',
        'address',
        'zipcode',
        'city',
        'state',
        'country',
    )
    search_fields = ('slug', 'streetname', 'donor__user__email')
