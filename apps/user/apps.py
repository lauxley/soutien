from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'user'

    def ready(self):
        # We're going to use some signals
        from . import signals
