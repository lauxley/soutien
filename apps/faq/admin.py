# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import FAQItem


@admin.register(FAQItem)
class FAQItemAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'question',
        'abstract_answer',
        'answer',
        'featured',
    )
    list_filter = ('featured',)
