from modeltranslation.translator import translator, TranslationOptions
from .models import FAQItem

class FAQItemTranslationOptions(TranslationOptions):
    fields = ('question',
              'abstract_answer',
              'answer')

translator.register(FAQItem, FAQItemTranslationOptions)
