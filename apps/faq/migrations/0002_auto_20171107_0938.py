# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-07 09:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='faqitem',
            name='abstract_answer_en',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='faqitem',
            name='abstract_answer_fr',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='faqitem',
            name='answer_en',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='faqitem',
            name='answer_fr',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='faqitem',
            name='question_en',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='faqitem',
            name='question_fr',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='faqitem',
            name='abstract_answer',
            field=models.TextField(blank=True, null=True),
        ),
    ]
