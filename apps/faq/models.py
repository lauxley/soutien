from django.db import models

# Create your models here.
class FAQItem(models.Model):
    """
    This model is used for Frequently Asked Questions
    """

    #: Question is the question to answer for
    question = models.CharField(max_length=200)

    #: Abstract of the answer
    abstract_answer = models.TextField(blank=True, null=True)

    #: Full answer
    answer = models.TextField()

    #: Do we need to display this item on the front page
    featured = models.BooleanField()

    #: We might provide a link to have more insight about a topic
    link = models.CharField(max_length=200,
                            blank=True,
                            null=True)

    #: This is used as css class to wrap them
    css_class = models.CharField(max_length=200,
                                 blank=True,
                                 null=True)
