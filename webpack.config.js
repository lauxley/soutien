const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, 'static/src'),
  entry: {
    app: './admin.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: 'css-loader?importLoaders=1!postcss-loader'
        }),
      },
      {
        test: /\.(woff|woff2|ttf|eot|svg)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
        use: 'file-loader?name=./fonts/[name]-[hash].[ext]'
      },
    ]
  },
  output: {
    path: path.resolve(__dirname, 'static/dist'),
    filename: "./admin.js"
  },
  plugins: [
    new ExtractTextPlugin('[name].bundle.css'),
  ],
};
