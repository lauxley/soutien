from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap

from .sitemaps import StaticViewSitemap

sitemaps = {'campaign': StaticViewSitemap,}

urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^', include('campaign.urls')),
    url(r"^admin/", admin.site.urls),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^perso/', include('perso.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^manager/', include('manager.urls')),
    url(r'^payment/', include('payment.urls')),
    # This is where we ill store the sitemap.xml
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap')
]

if settings.DEBUG:
    # Serve static files in DEBUG mode
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )

    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
