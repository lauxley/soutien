"""
Import settings from all files in settings directory
"""

from .env import *  # noqa
from .base import *  # noqa
from .apps import *  # noqa
from .auth import *  # noqa
from .static import *  # noqa
from .i18n import *  # noqa
from .local import * # noqa
from .hosts import * # noqa
