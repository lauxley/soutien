"""
This file contains local settings.
"""

SIZES_CHOICES = (
        ('Regular', (
            ('RS', 'Reg. Small'),
            ('RM', 'Reg. Medium'),
            ('RL', 'Reg. Large'),
            ('RXL', 'Reg. Extra Large'),
            ('RXL', 'Reg. Extra Extra Large'),
            ),
        ),
        ('Curvy', (
            ('CXS', 'Cur. Extra Small'),
            ('CS', 'Cur. Small'),
            ('CM', 'Cur. Medium'),
            ('CL', 'Cur. Large'),
            ('CXL', 'Cur. Extra Large'),
            ),
        ),
    )
