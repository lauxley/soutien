"""
We need to define ALLOWED_HOSTS in case of debug of if we're
locally running. If we have an env variable DJANGO_ALLOWED_HOSTS
defined, we can use it to.
"""

import os

from .env import DEBUG

if DEBUG:
    ALLOWED_HOSTS = ['*']
elif 'DJANGO_ALLOWED_HOSTS' in os.environ:
    ALLOWED_HOSTS = [os.environ['DJANGO_ALLOWED_HOSTS']]
