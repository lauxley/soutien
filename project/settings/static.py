"""
Static files related settings
See https://docs.djangoproject.com/en/1.10/howto/static-files/ for refs
"""

import os
from .base import BASE_DIR

# Url to get static files
STATIC_URL = "/static/"
# Directory of static files (used by collectstatic)
STATIC_ROOT = os.path.join(BASE_DIR, "static", "static_root")
# Additionaly to apps/static dirs, files in staticfiles_dirs will be copied
# by collectstatic
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static", "build"),
    os.path.join(BASE_DIR, "static", "dist"),
    os.path.join(BASE_DIR, "static", "img"),
)

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "static", "media")
