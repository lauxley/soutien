"""
Django installed apps
"""
from raven.transport.requests import RequestsHTTPTransport
from .env import DEBUG

DJANGO_APPS = [
    # We need to have this above admin
    "modeltranslation",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django.contrib.sitemaps",
]

CONTRIB_APPS = [
    "django_extensions",  # http://django-extensions.readthedocs.io/
    "crispy_forms",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "django_fsm",
    "django_filters",
    "django_tables2",
    "raven.contrib.django.raven_compat",  # for sentry
]

if DEBUG:
    CONTRIB_APPS.append(
        'debug_toolbar',  # https://django-debug-toolbar.readthedocs.io/
    )


PROJECT_APPS = [
    "campaign",
    "lqdn",
    "user",
    "payment",
    "perso",
    "manager",
    "faq",
]

INSTALLED_APPS = DJANGO_APPS + CONTRIB_APPS + PROJECT_APPS

CRISPY_TEMPLATE_PACK = "bootstrap4"


RAVEN_CONFIG = {
    'dsn': 'https://3e079f8121754f459d555f0418c7b10e:4395b97e93e244e2b47b8d1ee7bfa0c9@sentry.lqdn.fr/2',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    # 'release': raven.fetch_git_sha(os.path.abspath(os.pardir)),
    # The default transport issue SSL error with LE certs
    'transport': RequestsHTTPTransport,
}
