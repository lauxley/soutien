"""
User registration and login related settings
"""

AUTH_USER_MODEL = "auth.User"
EXTENDED_USER_MODEL = "userprofile.Profile"
LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/perso/"


AUTHENTICATION_BACKENDS = [
    # Default
    "django.contrib.auth.backends.ModelBackend",
]

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": ("django.contrib.auth.password_validation."
                 "UserAttributeSimilarityValidator"),
    },
    {
        "NAME": ("django.contrib.auth.password_validation."
                 "MinimumLengthValidator"),
    },
    {
        "NAME": ("django.contrib.auth.password_validation."
                 "CommonPasswordValidator"),
    },
    {
        "NAME": ("django.contrib.auth.password_validation."
                 "NumericPasswordValidator"),
    },
]


# Disable two steps logout
ACCOUNT_LOGOUT_ON_GET = True
